$(document).ready(function () {
    // if(window.chrome)
    //     window.directivesLoaded=true;
    router.initialiseRoutes(routes);
    //router.page('transaction');
    $('.comment-form textarea').textPlaceholder();
    notificationService.init(env.notificationConfig);
    accountService.validateUser().then(function (data) {

        if (data.status == 'SUCCESS' || data.status == null) {
            env.agentName = data.lastName + " " + data.firstName;
            $('#backOfficeAgentName').text(env.agentName);
            accountService.setUserData(data);
            if (window.location.href.indexOf('#') > -1) {
                var s = window.location.href.split('#');
                var path = s[s.length - 1];
                if (path.length > 2)
                    //rbac.getRbacDetails().then(function(data){
                    router.page(s[s.length - 1]);
                //});
                else {
                    //rbac.getRbacDetails().then(function(data){
                    router.page('transaction');
                    //});
                }
            }
            else {
                // rbac.getRbacDetails().then(function(data){
                router.page('transaction');
                //});
            }
        }
        else {
            router.page('login');
            //  accountService.setUserData({"status":"SUCCESS","errorCode":null,"errorCodeDescription":null,"userId":"merchantuser@gmail.com","password":"IyqIbfmvmGF/THjd8tkDaGnoVN0=","oldPassword":null,"merchant":"merchantuser","lastLoginDate":"03/14/2017 10:13:07","roleList":[{"id":15,"userType":"user","department":"merchant"}]});
            //  rbac.getRbacDetails();
            // router.page('transaction');
        }
    }, function () {
        router.page('login');
    });
    initialise();


    //rbac.getRbacDetails();
});

$(window).scroll(function () {
    filters.setDropDownHeight();
});
