
$(window).load(function() {
	$('.comment-form textarea').textPlaceholder();
	$('.comment-form textarea').val("");
	$('.comment-form textarea').each(function(){
		if (this.value.length >= 1) {
			$(this).closest('.comment-form').find('.btn-primary').removeAttr('disabled');
		} else {
			$(this).closest('.comment-form').find('.btn-primary').attr('disabled', 'disabled');
		}
	});
	calcSearchWidth();
	calcInfoSummaryWidth();
});
$(window).resize(function(){
	$('.modal-content').css('max-height', $(window).outerHeight() - 40);
	calcSearchWidth();
	//calcInfoSummaryWidth();
	//destroyNiceScroll();
});
$(document).on('mouseup touchend ',function (e){
	var container = $('.table-dropdown, .daterangepicker, .search-box');
	if (!container.is(e.target) && container.has(e.target).length === 0){

		$('.search-box').removeClass('opened');
		$('.search-box').find('.search-collapse').addClass('closed');
	}
});
function calcSearchWidth() {
	var $this = $(this),
		$screenWidth = $('body').width(),
		$subtraction = 0;
	if ($(window).width() >= 480 && $(window).width() <= 767) {
		$subtraction = 200;
	} else{
		if ($(window).width() <= 479) {
			$subtraction = 61;
		}
	}
	console.log($subtraction);
	$('.search-box.opened .search-collapse').outerWidth($screenWidth - $subtraction);
}
function calcInfoSummaryWidth() {
	var $this = $(this),
		$screenWidth = $('body').width(),
		$subtraction = 78;
	$('.summary-box .info-summary').outerWidth($screenWidth - $subtraction);
}

function destroyNiceScroll() {
	if ($(window).width() < 1025) {
		$('.scroll-table-modal .dataTables_scrollBody').niceScroll().remove();
		$('.scroll-table-modal .modal-content').niceScroll().remove();
		$('.modal-content').niceScroll().remove();
		$('#sidebar .scrollbar').niceScroll().remove();
	}
}

$(document).ready(function(){

    $('.only-date').daterangepicker({
        "linkedCalendars": false,
        "autoUpdateInput": true,
        "showDropdowns": false,
        "singleDatePicker": true,
        "showCustomRangeLabel": false,
        "buttonClasses": "btn",
        "applyClass": "btn-primary",
        "cancelClass": "btn-link",
        "drops": "up"
    });

    //$('#chargeBackReason').selectpicker('refresh');
    $('.account-select').selectpicker({
        size: 6
    });

   /* $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = Math.max.apply(null, Array.prototype.map.call(document.querySelectorAll('*'), function(el) {
                return +el.style.zIndex;
            })) + 10;
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });*/
})
