var directives = function (data) {
    var importHTML = function () {
        var link = document.createElement('link');
        link.rel = 'import';
        link.href = data.url;
        //link.setAttribute('async', ''); // make it async!
        link.onload = function (e) {
            createElement(link.import.querySelector('template').innerHTML);         
        };
        link.onerror = function (e) {
            console.log("importing " + data.url + " failed");
        };
        document.head.appendChild(link);
    }
    var createElement = function (content) {
        var proto = Object.create(HTMLElement.prototype, {
            createdCallback: {
                value: function () {
                    // var t = document.querySelector('#sdtemplate');
                    //var clone = document.importNode(content, true);
                    //this.createShadowRoot().appendChild(clone);
                    this.innerHTML = content;
                    console.log(data.directiveName);
                    if(data.directiveName=='login-footer' || data.directiveName == 'reporting-footer'){
                         window.directivesLoaded = true;
                        // loginForm.init();
                    }
                }
            }
        });
        document.registerElement(data.directiveName, { prototype: proto });
    }
    var registerDirective = function () {
        //console.log("register directive"+ data.directiveName);
        importHTML();
    }
    return {
        registerDirective: registerDirective
    }
};
