

function getData(requestData,type,url){

    var config = {};
    config.type = type;
    config.url=url;
    config.data = JSON.stringify(requestData);
    config.contentType = "application/json";
    return $.ajax(config);

}

