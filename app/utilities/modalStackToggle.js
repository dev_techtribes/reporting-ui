(function ($) {
    var oldModalFunc = $.fn.modal;
    var modalStack = [];
    console.log(modalStack);
    $.fn.modal = function (action) {

        if (typeof action != 'undefined') {
            if (action == 'hide') {
                if (modalStack.length > 1) {
                    $('body').addClass("no-scroll");
                    var current = modalStack.pop();
                    var show = modalStack[modalStack.length - 1];
                    oldModalFunc.call(this, 'hide');
                    $(".modal-backdrop").remove();

                    return oldModalFunc.apply(show, ['show']);
                }
                $('body').removeClass("no-scroll");
                if (modalStack.length == 1) {
                    modalStack = [];
                }
                $(".modal-backdrop").remove();
                return oldModalFunc.call(this, 'hide');
            }
            $('body').addClass("no-scroll");
            for (var i = 0; i < modalStack.length; i++) {
                oldModalFunc.call(modalStack[i], 'hide');
            }
            modalStack.push(this);
            $(".modal-backdrop").remove();
            return oldModalFunc.call(this, action);
        }
        else {
            for (var i = 0; i < modalStack.length; i++) {
                oldModalFunc.call(modalStack[i], 'hide');
            }
            $(".modal-backdrop").remove();
            modalStack.push(this);
            return oldModalFunc.call(this);
        }

    };
})(jQuery);
