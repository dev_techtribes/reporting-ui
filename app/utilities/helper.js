function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function formatAmounts(number)
{
    if(number && number != '****' && number != 'N/A')
    {
        var formattedAmount = parseFloat(number).toLocaleString();
        return addZeroes(formattedAmount);
    }
    return number;

}

function addZeroes( num ) {

    if(num.indexOf('.') === -1) {
        var value = num.replace(/\,/g,'');;
        var res = num.split(".");
        value = parseFloat(value);
        num =   value.toLocaleString()+".00";
    }
    return num
}
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
