var router = (function () {
    var routes = {};
    var initialiseRoutes = function (route) {
        routes = route;
    };
    var whenUserIdle=function(){
        txAction.logout();
        window.location.href = env.baseUrl + '/#login';
    };
    var startSession = function () {
        var idleTimer;
        function resetTimer() {
            clearTimeout(idleTimer);
            idleTimer = setTimeout(whenUserIdle, env.idleSeconds * 1000);
        }
        $(document.body).bind('mousemove keydown click', resetTimer); //space separated events list that we want to monitor
        resetTimer();
    };
    var page = function (path) {
        if (router.onRedirect)
            onRedirect();
        if (routes[path] == undefined) {
            if (user.isAuthenticated)
                path = 'transaction';
            else
                path = 'login';
        }
        if (path == "transaction")
            startSession();
        history.pushState({}, "", '#' + path);
        $(".router-view").html(routes[path].template);
        if (routes[path].onload) {
            var s = setInterval(function () {
                if (window.directivesLoaded) {
                    clearInterval(s);
                    routes[path].onload();
                }
            }, 50);
        }
    };
    return {
        initialiseRoutes: initialiseRoutes,
        page: page
    }
})();