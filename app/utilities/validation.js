var validations = (function () {
    validate = function (ele) {
     //   alert('good mrng');
         $(ele).removeClass('error');
        if (ele.dataset.compare == 'true') {
            ele2 = document.getElementById(ele.dataset.controlid);
            if (ele2.value != ele.value) {
                $(ele).removeClass('error');

                if(!$(ele2).hasClass("validation-error")){
                 //   alert('sweet');
                     $(ele).removeClass('error');
                      $(ele).next().remove();
                    ele.setCustomValidity("Passwords Don't Match");
                    //ele.setCustomValidity('');
                }    
            } else {
              //  alert('heello');
                ele.setCustomValidity("");
            }
        }

        var msg=ele.validationMessage;
        if (ele.validationMessage.indexOf('format') > -1 && $(ele).attr('type') == 'password') {
          if($(ele).val().match(" ")) {
          
            msg='Unsupported symbols or Elements in the password';
          }
          else{
           
            msg='Password should contain 1 uppercase, 1 lowercase, 1 digit and 1 special character with min 8 and max 20 length';
          }
        }
        if (ele.checkValidity() == false && !$(ele).hasClass('error')) {
             $(ele).removeClass('error');
             $(ele).removeClass("validation-error");
            $(ele).next().remove();
            $('<label class="error">' + msg + '</label>').insertAfter($(ele));
            $(ele).addClass('error');
            $(ele).attr('data-isvalid', 'false');
        }
        else if (ele.checkValidity() == false && $(ele).hasClass('error')) {
            $(ele).next().text(msg);
        }
        else if (ele.checkValidity()) {
            $(ele).removeClass('error');
             $(ele).removeClass("validation-error");
            $(ele).next().remove();
            $(ele).attr('data-isvalid', 'true');
        }

    };

    reset = function (ele) {    
            $(ele).removeClass('error');
            $(ele).removeClass("validation-error");
            $(ele).next().remove();
            $(ele).attr('data-isvalid', 'true');
    };

    resetValidation = function (ele) {
        //reset(ele);
        $(ele).find('*').filter("input:not([type='submit'],[type='button'])").each(function (e) {
            reset(this);
        });
    }

    validateForm = function (ele) {
        var valid = true;
        $(ele).find('*').filter("input:not([type='submit'],[type='button'])").each(function (e) {
            validate(this);
            if (this.dataset.isvalid == 'false')
                valid = false;
        });
        return valid;
    };



   
    validateOnBlur = function(ele)
    {
        console.log("blur")
        $(ele).blur(function () {
            if (ele.dataset.compare == 'true') {
                ele2 = document.getElementById(ele.dataset.controlid);
                if (ele2.value != ele.value) {
                    ele.setCustomValidity("Passwords Don't Match");
                } else {
                    ele.setCustomValidity('');
                }
            }
        })
    }
    var init = function () {
        $(".login input:not([type='submit'],[type='button'])").keyup(function () {
            if ($(this).hasClass('error')) {
                validate(this);
            }
        });
        $(".login input:not([type='submit'],[type='button'])").blur(function () {
            validate(this);
        });

        $("#cpswd").blur(function () {
         /*   e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            e.cancelBubble = true;*/
            var ele = this;
            if (ele.dataset.compare == 'true') {
                ele2 = document.getElementById(ele.dataset.controlid);
                if (ele2.value != ele.value) {
                   if(!$(ele).hasClass("validation-error"))
                   {
                     //  alert("want sweet");
                    $(ele).removeClass('error');
                      $(ele).next().remove();
                       $(ele).addClass("validation-error");
                       $('<label class="validation-error" id="blurValidations">' + "Passwords Don't Match" + '</label>').insertAfter($(ele));
                       //$(ele).attr('data-isvalid', 'false');
                   }
                } else {
                    if($(ele).hasClass("validation-error")) {
                        $(ele).removeClass("validation-error");
                        $(ele).removeClass('error');
                      $(ele).next().remove();
                        $(ele).attr('data-isvalid', 'true');
                    }
                }
            }
         
        })

        $("#pswd").blur(function () {
         /*   e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            e.cancelBubble = true;*/
            var ele = this;            
            if (ele.validationMessage.indexOf('format') > -1 && $(ele).attr('type') == 'password') {

              if($(ele).val().match(" ")) {

                 
                   $(ele).removeClass("validation-error");
                  $(ele).removeClass('error');
                    $(ele).next().remove();
                     $(ele).addClass("validation-error");
                     $('<label class="validation-error" id="blurValidations1">' + "Unsupported symbols or Elements in the password" + '</label>').insertAfter($(ele));
                     //$(ele).attr('data-isvalid', 'false');

               }
               else{

                 
                   $(ele).removeClass("validation-error");
                  $(ele).removeClass('error');
                    $(ele).next().remove();
                     $(ele).addClass("validation-error");
                     $('<label class="validation-error" id="blurValidations">' + "Password should contain 1 uppercase, 1 lowercase, 1 digit and 1 special character with min 8 and max 20 length" + '</label>').insertAfter($(ele));
                     //$(ele).attr('data-isvalid', 'false');
                 
             }
            } 

              else{              

                if($(ele).hasClass("validation-error")) {
                    $(ele).removeClass("validation-error");
                    $(ele).removeClass('error');
                  $(ele).next().remove();
                    $(ele).attr('data-isvalid', 'true');
                }
            }
            
         
        })
    


    };
    return {
        init: init
    }
})();
