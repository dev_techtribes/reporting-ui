$(document).ajaxSend(function() {
    $('#loader-container').height($('body').height())
    $('#loader-container').fadeIn('fast');
    $('body').css("overflow","hidden");
});

$(document).ajaxComplete(function() {
    $('body').css("overflow","auto");
    setTimeout(function () {
        $('#loader-container').fadeOut('fast');
    }, 300);
});

// $(document).ready(loadtran4);

var initialise=function () {
    setTimeout(function () {
        $('#loader-container').fadeOut('fast')
        setTimeout(function () {
            $('.tools-panel').show();
            $('.tools-panel').animateCss('slideInDown');
        }, 100);
    }, 400);

    window.onbeforeunload = function (e) {
        $('.tools-panel').slideUp(300, function() {
            $('#loader-container').show();
            delay(350);
        });
    }

    function delay(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    }

    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });

};
