var routes={
    'ipblock':{
        template:'<ip-block></ip-block>'
    },
    'login':{
        template:'<login-content></login-content>',
        onload:loginForm.init
    },
    'transaction':{
        template:'<db-content-area></db-content-area>',
        onload:transactionTable.init
    }
};