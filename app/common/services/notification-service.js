var notificationService = (function () {


    var init = function(config)
    {
        $.notifyDefaults(config);
    };

    var notifySuccess = function(message)
    {
         var configuration = {};
         configuration.options = {
             "message": message
         };
         configuration.settings = {
               "type":"success"
         };
         $.notify(configuration.options,configuration.settings);
    };

    var notifyDanger = function(message)
    {
        var configuration = {};
        configuration.options = {
            "message": message
        };
        configuration.settings = {
            "type":"danger",
             "allow_dismiss": true,
            "mouse_over": "pause"
        };
        $.notify(configuration.options,configuration.settings);
    };


    return {
        init: init,
        notifySuccess:notifySuccess,
        notifyDanger:notifyDanger
    };
})();
