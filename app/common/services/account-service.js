var accountService = (function () {
    var validateUser = function () {
        var url = env.baseUrl + env.getUserDataUrl;
        var ajax = getData({}, "POST", url);
        return ajax.then(function (data) {
            console.log(data);
            return data;
        }, function (data) {
            return data;
        })
    };
    var setUserData=function(data){
        console.log(data);
        user.userid=data.userId;
        env.getTransactionsRequestObject.merchant=data.merchant;
        user.isAuthenticated=true;
        user.lastloginDate=data.lastLoginDate;
        user.merchant=data.merchant;
        user.department=data.department;
        user.isMerchant=false;
        for (var role in data.roleList){
            if(data.roleList[role].department=='merchant'){
                user.isMerchant=true;
                break;
            }
        }
        if(data.isInternal=='yes')
            user.isAdmin=true;
        else
            user.isAdmin=false;
        /*if(user.isMerchant){
           $('#sidebar li.tran-merchant').attr('style', 'display:block');
           $('#sidebar li.tran-brand').attr('style', 'display:block');
           $('#sidebar li.tran-mid').attr('style', 'display:block');
            env.optionalHeaderListTxInfo.push('merchant');
            env.optionalHeaderListTxInfo.push('brand');
            env.optionalHeaderListTxInfo.push('mid');
            $('#sidebar li.tran-merchant .checkbox input').attr('checked',false);
            $('#sidebar li.tran-brand .checkbox input').attr('checked',false);
            $('#sidebar li.tran-mid .checkbox input').attr('checked',false);
        }
        else{
            $('#sidebar li.tran-merchant').attr('style', 'display:none');
            $('#sidebar li.tran-brand').attr('style', 'display:none');
            $('#sidebar li.tran-mid').attr('style', 'display:none');
            $('#sidebar li.tran-merchant .checkbox input').attr('checked','checked');
            $('#sidebar li.tran-brand .checkbox input').attr('checked','checked');
            $('#sidebar li.tran-mid .checkbox input').attr('checked','checked');
        }*/
    }
    
    var changePassword = function (requestData) {
        var url = env.baseUrl + env.changePswdUrl;
        var ajax = getData(requestData, "POST", url);
        return ajax.then(function (data) {
            console.log(data);
            return data;
        }, function (data) {
            return data;
        })
    };
    var login = function (requestData) {
        return $.getJSON(env.getIp).then(function (data) {
            requestData.ip = data.ip;
            var url = env.baseUrl + env.loginUrl;
            var ajax = getData(requestData, "POST", url);
            return ajax.then(function (data) {
                console.log(data);
                return data;
            }, function (data) {
                return data;
            });
        });
    };
    return {
        login: login,
        changePassword: changePassword,
        validateUser:validateUser,
        setUserData:setUserData
    };
})();
