var masterService=(function(){

    var getProfiles=function(){
        var url = env.baseUrl+env.profileUrl;
        var ajax =getData("", "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var getMerchants=function(){
        var url = env.baseUrl+env.merchantUrl;
        var ajax =getData("", "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var getMids=function(){
        var url = env.baseUrl+env.midUrl;
        var ajax =getData("", "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };

    var getCurrencies=function(){
        var url = env.baseUrl+env.currencyUrl;

        var ajax = getData("", "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var getTransactionTypes=function(){
        var url = env.baseUrl+env.getTransactionTypesUrl;
        var ajax = getData("", "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };

    var getTransactionStatus = function(){
        var url = env.baseUrl+env.getTransactionStatusUrl;
        var ajax = getData("", "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };

    var getPaymentInstruments=function(){
        var url = env.baseUrl+env.getPaymentInstrumentsUrl;
        var ajax = getData("", "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };

    return{
        getProfiles:getProfiles,
        getMids:getMids,
        getMerchants:getMerchants,
        getCurrencies:getCurrencies,
        getTransactionTypes:getTransactionTypes,
        getPaymentInstruments:getPaymentInstruments,
        getTransactionStatus :getTransactionStatus
    };
})();
