var observationsService = (function () {


    var getObservations = function(txnRef)
    {
        var url =  env.baseUrl + env.getObservationsUrl + "/"+txnRef;
        var ajax = getData("", "POST", url);
        return ajax.then(function(data){
            console.log(data);
               return data;
        })
    };

    var editObservations = function(txnRef,id,data)
    {
        var requestData = jQuery.extend({},env.observationsRequestObject);
        requestData.action = "update";
        requestData.value = data;
        requestData.id = id;
        requestData.transactionReference  = txnRef;
        var url =  env.baseUrl + env.updateObservationsUrl;
        var ajax =  getData(requestData,"POST",url)
        return ajax.then(function (data) {
                return data;
        })
    }

    var insertObservations = function (txnRef,data) {
        var requestData = jQuery.extend({},env.observationsRequestObject);
        requestData.action = "insert";
        requestData.value =  data;
        requestData.transactionReference  = txnRef;
        var url =  env.baseUrl + env.updateObservationsUrl;
        var ajax =  getData(requestData,"POST",url);
        return ajax.then(function (data) {
            return data;
        })
    }

    var deleteObservations = function (txnRef,id) {
        var requestData = jQuery.extend({},env.observationsRequestObject);
        requestData.action = "delete";
        requestData.transactionReference  = txnRef;
        requestData.id = id;
        var url =  env.baseUrl + env.updateObservationsUrl;
        var ajax =  getData(requestData,"POST",url)
        return ajax.then(function (data) {
            return data;
        })
    }
    return {
        getObservations: getObservations,
        editObservations: editObservations,
        insertObservations:insertObservations,
        deleteObservations:deleteObservations
    };
})();
