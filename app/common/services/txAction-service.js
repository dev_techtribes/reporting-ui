var txAction = (function () {
    var exportTablecsv = function (requestData) {

        var url = env.baseUrl+env.exportTransactionInfoUrl;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var refund = function (requestData) {

        var url = env.baseUrl+env.getRefundUrl;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var availableTxnStatusList = function (requestData) {

        var url = env.baseUrl+env.getAvailableTxnStatusUrl;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var providerErrorList = function (requestData) {

        var url = env.baseUrl+env.getProviderErrorsUrl;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var updatetxnstatus = function (requestData) {

        var url = env.baseUrl+env.getUpdateTxnStatusUrl;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var blacklist = function(requestData){
        var url = env.baseUrl+env.blacklistUrl.replace('<TRANSACTION_REF>',requestData.ref);
        var ajax = getData({}, "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var fraud = function(requestData){
        var url = env.baseUrl+env.fraudUrl;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    var logout = function () {
        var url = env.baseUrl + env.logoutUrl;
        var ajax = getData({}, "POST", url);
        return ajax.then(function (data) {
            console.log(data);
            return data;
        }, function (data) {
            return data;
        })
    };

    var addChargeback = function (reqData) {
        var url =  env.baseUrl + env.createChargebackUrl;
        var ajax =  getData(reqData,"POST",url);
        return ajax.then(function (data) {
            return data;
        })
    };

    var getChargebackReasons = function(txnRef)
    {
        var url =  env.baseUrl + env.getChargebackReasonsUrl + "/"+txnRef;
        var ajax = getData("", "POST", url);
        return ajax.then(function(data){

            return data;
        })
    };
    return {
        exportTablecsv: exportTablecsv,
        refund: refund,
        availableTxnStatusList:availableTxnStatusList,
        providerErrorList:providerErrorList,
        updatetxnstatus:updatetxnstatus,
        blacklist:blacklist,
        fraud:fraud,
        logout:logout,
        addChargeback:addChargeback,
        getChargebackReasons:getChargebackReasons

    };
})();
