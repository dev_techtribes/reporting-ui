var transactionService = (function () {

    var getTransactionInfo = function (requestData) {

        var url = env.baseUrl+env.getTransactionInfoUrl;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            $.each(data.transactions, function(key, value){
                $.each(value, function(internalKey, internalValue){
                    for(var innerkey in internalValue){                                              
                        if(innerkey == 'txnAmount' && internalValue['txnAmount'] != null){
                            internalValue['txnAmount'] = internalValue['txnAmount'].toFixed(2);
                        }
                        else if(innerkey == 'providerTxnId' && internalValue['providerTxnId'] != null && internalValue['providerTxnId'] == -1){
                            internalValue['providerTxnId'] = "N/A";
                        }
                        else if(internalValue[innerkey] === false || internalValue[innerkey] === 0){
                            continue;
                        }
                        else if(!internalValue[innerkey] || (typeof internalValue[innerkey] === 'string' && internalValue[innerkey].trim() == '')){
                            internalValue[innerkey] = "N/A";
                        }
                    }                   
                });
            });
            return data;
        })
    };
    var getBlackListParameters = function(requestData){
        var url = env.baseUrl +env.getBlackListParameters;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            /*console.log(data);*/
            return data;
        })
    };
    var getTxnLogs = function (txnRef) {

        var url = env.baseUrl+env.getTransactionLogsUrl+"/"+txnRef;
        var ajax = getData("", "POST", url);

        return ajax.then(function (data) {
            console.log(data);
            return data;
        })
    };
    return {
        getTxnLogs:getTxnLogs,
        getTransactionInfo: getTransactionInfo,
        getBlackListParameters:getBlackListParameters
    };
})();
