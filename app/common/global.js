
window.env = (function (_env) {
    if (_env == null) {
        _env = {

            //urls

            //domainlUrl:'http://dlb-reporting.cloudapp.net',
            domainUrl:'http://localhost:8080/reportingshiro-1.0',
            baseUrl: '/api',

            rbacUrl:  '/authz/getRbacDetails',
            profileUrl: '/master/getProfiles',
            merchantUrl: '/master/getMerchants',
            currencyUrl: '/master/getCurrencies',
            getTransactionTypesUrl: '/master/getTransactionTypes',
            getTransactionStatusUrl: '/master/getTransactionStatus',
            getPaymentInstrumentsUrl: '/master/getPaymentInstruments',
            getTransactionInfoUrl: '/reports/getTransactionInfo',
            exportTransactionInfoUrl: '/reports/generateReport',

            getObservationsUrl : '/useraction/fetchObservations',
            getRefundUrl:'/useraction/initiateRefund',
            getUpdateTxnStatusUrl:'/reports/updateTxnStatus',
            getAvailableTxnStatusUrl:'/master/getAvailableTxnStatus',
            getProviderErrorsUrl:'/reports/getTxnProviderErrors',
            createChargebackUrl:'/useraction/doChargeback ',
            midUrl:'/master/getMid',
            getChargebackReasonsUrl : '/reports/getChargebackReason',
            activeFilterList:{},

            getTransactionLogsUrl : "/reports/getTransactionLogs",
            updateObservationsUrl : "/useraction/updateNotes",

            loginUrl:"/user/auth",
            getUserDataUrl:"/user/getUserInfo",
            changePswdUrl:"/user/changePassword",
            getIp:"http://jsonip.com/?callback=?",
            blacklistUrl: '/useraction/markCustomerBlacklist/<TRANSACTION_REF>/4',
            fraudUrl:'/useraction/markTransactionFraud',
            logoutUrl:'/user/logout',
            notificationConfig: { "z_index" : 2000,"delay": 2000 },
            ipBLock:"IP_NOT_ALLOWED",
            getBlackListParameters:'/reports/getBlacklistCustomers',
            idleSeconds:1799,
            //transaction Table Data
            trxTableCollection:{},
            tempCollection:{},
            tempTransactionsRequestObject:{},

            //API -getTransactionInfo Request
            getTransactionsRequestObject:{"txnType":null,"startDate":"","endDate":"","index":1,"size":25,"currencyList":[],"dalberryTxnReference":"","merchantTxnId":"","minTxnAmount":"","maxTxnAmount":"","methodList":[],"txnStatusList":[],"customerId":"","merchantCustomerId":"","merchant":"","merchantList":[],"userId":"","midList":[],"profileList":[],"sortByColumnName":null,"sortType":"DESC"},

            //API - observationsInfo Request

            observationsRequestObject: {"field":"null","value":"null","action":"null","transactionReference":"null","ipAddress":"null","agent":"reporting-user","transactionId":"null","source":"null","noteStatus":"null"},
            //transaction optional columns
            optionalHeaderListTxInfo : ["type", "paymentInstrument", "txnCreatedDate", "cardHolderName", "cardBrand", "paymentMethod", "ip", "3dSecure", "billingDescription",  "revertedTxn", "platform","browser", "providerTxnID","authCode", "errorCode","errorMessage","dbErrorMessage","dbErrorCode"],
            optionalHeaderListTxInfoCloned : ["type", "paymentInstrument", "txnCreatedDate", "cardHolderName", "cardBrand", "paymentMethod", "ip", "3dSecure", "billingDescription",  "revertedTxn", "platform","browser", "providerTxnID","authCode", "errorCode","errorMessage","dbErrorMessage","dbErrorCode"],
            //customer optional columns
            optionalHeaderListCustInfo : ["firstName", "lastName", "customerEmail", "issuingBank", "issuingCountry", "phone", "birthDate", "address", "city", "state", "country","isMale","mac","postalCode"],
            optionalHeaderListCustInfoCloned : ["firstName", "lastName", "customerEmail", "issuingBank", "issuingCountry", "phone", "birthDate", "address", "city", "state", "country","isMale","mac","postalCode"],
            //masterData
            getStatusMasterData : [],
            getCurrencyMasterData: [],
            getMethodMasterData : [],
            getTypeMasterData : [],
            getMerchantMasterData : [],
            getMidMasterData : [],
            getBrandMasterData : [],
            getAvailableTxnMasterData: [],
            getProviderErrorsData: [],
            //chargeback
            chargebackReasonsData : ["Services not provided or merchandise not received","Fraud Identification","Non-matching or invalid account number","Credit previously issued","Transaction amount differs","Duplicate Transaction","No Cardholder Authorization","Expired card","Transaction not recognized","Credit not processed","Fraud card absent environment","Not as described or defective merchandise","Incorrect currency or transaction code","Late presentment","Declined Authorization"],

        //search box labels
            searchBoxLabelCustomerId:'Customer ID',
            searchBoxLabelMethod:'Method',
            searchBoxLabelDLBRef:'Dalberry Reference',
            searchBoxLabelMerchantTxnId:'Merchant TXN ID',
            searchBoxLabelTxnStatus:'TXN STATUS',
            searchBoxLabelCcy:'Ccy',
            searchBoxLabelTxnType:'TXN type',

            //logs
            logsCommentNotAvailable : 'N/A',

            //observations
            observationsCommentAdded:'Comment Added',

            //agent firstName and lastName labels
            agentName:'',

            rbacCustInfoMap :
            {"str_FirstName":"firstname",
            "str_LastName":"lastName",
            "str_CustomerEmail":"customerEmail",
            "str_Phone":"phone",
            "dte_Birthdate":"birthDate",
            "str_Address":"address",
            "str_Country":"country",
            "str_City":"city",
            "str_ProvState":"state",
            "str_PostalCode":"postalCode",
            "str_mac_address":"mac",
            "bit_IsMale":"isMale"},
            rbacTxnInfoMap : {
                  "str_clientName":"merchant",
                  "str_Profile":"brand",
                  "str_transactionReference":"dalberryTxnReference",
                  "int_CustomerTransactionId":"merchantTxnId",
                  "merchant_customer_id":"merchantCustomerId",
                  "bit_is3dTransaction":"3dSecure",
                  "txn_type":"txnType",
                  "str_Status":"txnStatus",
                  "str_Processor":"mid",
                  "str_TrackCode":"providerTxnID",
                  "supplierErrorCode":"errorCode",
                  "supplierErrorMessage":"errorMessage",
                  "friendlyErrorCode":"dbErrorCode",
                  "friendlyErrorMessage":"dbErrorMessage",
                  "created_date":"txnCreatedDate",
                  "str_authCode":"authCode",
                  "processed_date":"txnProcessedDate",
                  "mon_Amount":"txnAmount",
                  "str_currencyCode":"txnCurrency",
                  "payment_method":"cardType",
                  "card_type" :"paymentMethod",
                  "payment_instrument" :"paymentInstrument",
                  "str_remote_IP" :"ip",
                  "str_descriptor" : "billingDescription",
                  "linkedTransactionRef" :"revertedTxn",
                  "browser" :"browser",
                  "platform" :"platform",
                  "userName" : "cardHolderName",
                  "str_channel" : "channel",
                  "str_subchannel" : "subchannel"
            },
            rbacHeaderInfoMap:
            {
                  "str_transactionReference":"dalberryTxnReference",
                  "str_Status":"txnStatus",
                  "int_CustomerTransactionId":"merchantTxnId",
                  "mon_Amount":"txnAmount",
                  "str_Profile":"brand",
                  "str_currencyCode":"txnCurrency",
                  "processed_date":"txnProcessedDate",
                  "txn_type":"txnType",
                  "payment_method":"cardType",
                  "merchant_customer_id":"merchantCustomerId",
                  "str_Processor":"mid",
                  "str_clientName":"merchant"
            },
            rbacSideBarLabels:{
                  "txnProcessedDate": "TXN processed date",
                  "merchantCustomerId":"Customer ID",
                  "customerId": "Customer ID",
                  "txnAmount": "Amount",
                  "txnCurrency": "Ccy",
                  "paymentMethod": "Payment Instrument type",
                  "txnType": "TXN type",
                  "txnStatus": "TXN status",
                  "dalberryTxnReference": "Dalberry reference",
                  "merchantTxnId": "Merchant TXN ID",
                  "merchant":"Merchant",
                  "brand":"Brand",
                  "mid":"MID"
            },

            sortableColumns:{
                  "txnProcessedDate":"DESC",
                  "dalberryTxnReference":"DESC",
                  "merchantTxnId":"DESC",
                  "merchantCustomerId":"DESC",
                  "txnCreatedDate":"DESC",
                  "providerTxnId":"DESC",
                  "authCode":"DESC",
                  "ip":"DESC",
                  "billingDescriptor":"DESC",
                  "customerId":"DESC",
                  "mac":"DESC",
                  "cardHolderName":"DESC",
                  "txnAmount":"DESC"
            },
            //pagination
            index:1,
            updatedByPagination:false,
            updatedByQuickSummary:1,
            stopRequest:false
        };
    }
    return _env;
})(window.env || null);
