window.user = (function (_user) {
    if(_user==null){
        _user={
            merchant:'',
            userid:'',
            isAuthenticated:false,
            department:'',
            isAdmin:false,
            isMerchant:false,
            lastloginDate:null
        };
    }
    return _user;
})(window.user || null);