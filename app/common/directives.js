window.addEventListener('WebComponentsReady', function () {
    var buildurl='';
    directives({
        url: buildurl + "/app/templates/db-content-area.html",
        directiveName: "db-content-area"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/header.html",
        directiveName: "reporting-header"
    }).registerDirective();



    //account
    directives({
        url: buildurl + "/app/templates/account/login-content.html",
        directiveName: "login-content"
    }).registerDirective();

    directives({
        url: buildurl + "/app/templates/account/login-header.html",
        directiveName: "login-header"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/account/login-modal.html",
        directiveName: "login-modal"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/account/pswd-recovery.html",
        directiveName: "pswd-recovery"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/account/recovery-fb.html",
        directiveName: "recovery-fb"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/account/change-pswd-modal.html",
        directiveName: "change-pswd-modal"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/account/login-footer.html",
        directiveName: "login-footer"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/ipblock.html",
        directiveName: "ip-block"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/notification-template.html",
        directiveName: "notification-template"
    }).registerDirective();


    //transactions
    directives({
        url: buildurl + "/app/templates/transactions/tools-panel.html",
        directiveName: "reporting-tools-panel"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/create-chargeback-modal.html",
        directiveName: "create-chargeback-modal"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/content-area.html",
        directiveName: "reporting-content-area"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/side-bar.html",
        directiveName: "reporting-side-bar"
    }).registerDirective();

    directives({
        url: buildurl + "/app/templates/transactions/change-password.html",
        directiveName: "change-password"
    }).registerDirective();

    directives({
        url: buildurl + "/app/templates/transactions/customer-info-slider.html",
        directiveName: "reporting-customer-info-slider"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/transaction-info-slider.html",
        directiveName: "reporting-transaction-info-slider"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/transaction-table.html",
        directiveName: "reporting-txn-table"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/summary-box.html",
        directiveName: "reporting-summary-box"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/tx-details.html",
        directiveName: "transaction-details"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/refund.html",
        directiveName: "refund-modal"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/update-txn-status.html",
        directiveName: "txnstatus-modal"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/blacklist.html",
        directiveName: "blacklist-modal"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/fraud.html",
        directiveName: "fraud-modal"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/transactions/observations.html",
        directiveName: "observations-area"
    }).registerDirective();
    directives({
        url: buildurl + "/app/templates/footer.html",
        directiveName: "reporting-footer"
    }).registerDirective();
});
