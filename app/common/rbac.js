var rbac = (function () {
    var editRole = 'EDIT',
    viewRole = 'VIEW',
    exportRole = 'EXPORT',
    blockRole = 'NONE',
    maskedRole = "MASKEDVIEW",
    roleEnum = {};
    roleEnum[maskedRole] = 5;
    roleEnum[editRole] = 4;
    roleEnum[exportRole] = 3;
    roleEnum[viewRole] = 2;
    roleEnum[blockRole] = 1;
    roles={};
    var blockElement=function(el){
        if(el.is("a")){
            el.addClass("blocked");
            el.parent().addClass("parent-blocked");
        }

        el.filter('.rbac-risk').addClass('blur');
        el.prop("disabled", true);
        el.find(".btn-link").each(function(){
            $(this).css("border-color","none!important");
        })
        el.prop("readonly", true);
        el.find('*').prop("disabled", true);
        el.find('*').filter('.rbac-risk').addClass('blur');
        el.find('*').filter('input').prop("readonly", true);
        el.find('*').filter('textarea').prop("readonly", true);
        el.find('*').filter('button').prop("disabled",true);
    };
    var hideElement=function(el){
        if(el.is("a")){
            el.addClass("blocked");
            el.parent().addClass("parent-blocked");
        }

        el.filter('.rbac-risk').parent().remove();
        el.prop("disabled", true);
        el.find(".btn-link").each(function(){
            $(this).css("border-color","none!important");
        })
        el.prop("readonly", true);
        el.find('*').prop("disabled", true);
        el.find('*').filter('.rbac-risk').parent().remove();
        el.find('*').filter('input').prop("readonly", true);
        el.find('*').filter('textarea').prop("readonly", true);
        el.find('*').filter('button').prop("disabled",true);
    };
    var updateRoles=function(){
        assignRoles(roles);
    };
    var getRbacDetails = function () {
        var requestData = { "userId": user.userid };
        var url = env.baseUrl + env.rbacUrl;
        var ajax = getData(requestData, "POST", url);

        return ajax.then(function (data) {
            rbac.roles=data;
            assignRoles(data);

            return data;
        },function(d){
            console.log(d);
        });
    };
    var updateTransactionPage = function (roles) {
        if(!jQuery.isEmptyObject(roles)){
        if (roles.accessType == blockRole) {
            $('reporting-tools-panel').css('display', 'none !important');
            $('reporting-content-area').css('display', 'none !important');
            $('reporting-side-bar').css('display', 'none !important');
            $('reporting-tools-panel').css('display', 'none !important');
        }
        else if (roles.accessType == viewRole) {
            blockElement($('#exportToCSV'));
            blockElement($("#blacklist-btn"));
            blockElement($('#refund-box'));
            blockElement($('.comment-form'));
            // $('#blacklist-btn').css('display', 'none !important');
            // $('#refund-box').css('display', 'none !important');
            // $('.comment-form textarea').css('display', 'none !important');
            // $('.comment-form footer').css('display', 'none !important');
            // $('.comment-form ul.tools').css('display', 'none !important');
        }
        else if (roles.accessType == exportRole) {
            // $('#blacklist-btn').css('display', 'none !important');
            // $('#refund-box').css('display', 'none !important');
            // $('.comment-form textarea').css('display', 'none !important');
            // $('.comment-form footer').css('display', 'none !important');
            // $('.comment-form ul.tools').css('display', 'none !important');
            blockElement($("#blacklist-btn"));
            blockElement($('#refund-box'));
            blockElement($('.comment-form textarea'));
            blockElement($('.comment-form footer'));
            blockElement($('.comment-form ul.tools'));
        }
        if (roles.subHierarchy != null && roles.subHierarchy.hasOwnProperty('customerInfo'))
            updateCustomerInfo(roles.subHierarchy.customerInfo, roleEnum[roles.accessType]);
        if (roles.subHierarchy != null && roles.subHierarchy.hasOwnProperty('transactionInfo'))
            updateTransactionInfo(roles.subHierarchy.transactionInfo, roleEnum[roles.accessType]);
        }
    };
    var updateCustomerInfo = function (roles, level) {
        //if (roles.accessType == blockRole) {
            //$('.transactions-area .transactions-table-container .transactions-table tr th:nth-child(3)').css('display', 'none !important');
           // blockElement($('.transactions-area .transactions-table-container .transactions-table tr td:nth-child(3)'));
            console.log("Roles :: "+JSON.stringify(roles));
            if (roles.subHierarchy != null) {
                for (s in roles.subHierarchy) {
                    if (roles.subHierarchy.hasOwnProperty(s)) {
                        if (roles.subHierarchy[s].accessType == blockRole) {
                            console.log(s);
                            hideElement($('.cust-' + env.rbacCustInfoMap[s]));
                        }
                    }
                }
            }
       // }
       // if (roles.accessType == maskedView) {
            //$('.transactions-area .transactions-table-container .transactions-table tr th:nth-child(3)').css('display', 'none !important');
           // blockElement($('.transactions-area .transactions-table-container .transactions-table tr td:nth-child(3)'));
            
            if (roles.subHierarchy != null) {
                for (s in roles.subHierarchy) {
                    if (roles.subHierarchy.hasOwnProperty(s)) {
                        if (roles.subHierarchy[s].accessType == maskedRole) {
                            blockElement($('.cust-' + env.rbacCustInfoMap[s]));
                        }
                    }
                }
            }
        //}
    };
    var updateTransactionInfo = function (roles, level) {
        /*if (roles.accessType == blockRole) {
            //$('.slick-arrow').css('display', 'none !important');
            // $('.transactions-area .transactions-table-container .transactions-table tr th').css('display', 'none !important');
            // $('.transactions-area .transactions-table-container .transactions-table tr td').css('display', 'none !important');
            // $('.transactions-area .transactions-table-container .transactions-table tr th:nth-child(3)').css('display', 'block !important');
            // $('.transactions-area .transactions-table-container .transactions-table tr td:nth-child(3)').css('display', 'block !important');
            //$('.tranInfo').css('display', 'none !important');
        }
        if (roles.subHierarchy != null) {
            for (s in roles.subHierarchy) {
                if (roles.subHierarchy.hasOwnProperty(s)) {
                    if (roles.subHierarchy[s].accessType == blockRole) {
                        //$('.tran-' + s).css('display', 'none !important');
                        blockElement($('.tran-' + s));
                    }
                }
            }
        }*/

        console.log("Roles :: "+JSON.stringify(roles));
            if (roles.subHierarchy != null) {
                for (s in roles.subHierarchy) {
                    if (roles.subHierarchy.hasOwnProperty(s)) {
                        if (roles.subHierarchy[s].accessType == blockRole) {
                            if(s == 'friendlyErrorCode'){
                              hideElement($('.tran-' + env.rbacTxnInfoMap[s]));
                              hideElement($('.tran-' + env.rbacTxnInfoMap['friendlyErrorMessage']));  
                            }else{
                                hideElement($('.tran-' + env.rbacTxnInfoMap[s]));
                            }
                        }
                    }
                }
            }
       // }
       // if (roles.accessType == maskedView) {
            //$('.transactions-area .transactions-table-container .transactions-table tr th:nth-child(3)').css('display', 'none !important');
           // blockElement($('.transactions-area .transactions-table-container .transactions-table tr td:nth-child(3)'));
            
            if (roles.subHierarchy != null) {
                for (s in roles.subHierarchy) {
                    if (roles.subHierarchy.hasOwnProperty(s)) {
                        if (roles.subHierarchy[s].accessType == maskedRole) {
                            if(s == 'friendlyErrorCode'){
                              blockElement($('.tran-' + env.rbacTxnInfoMap[s]));
                              blockElement($('.tran-' + env.rbacTxnInfoMap['friendlyErrorMessage']));  
                            }else{
                                blockElement($('.tran-' + env.rbacTxnInfoMap[s]));
                            }
                        }
                    }
                }
            }

    };
    var assignRoles = function (roles) {
        updateTransactionPage(roles.transactions);
    }

    return {
        roles:roles,
        getRbacDetails: getRbacDetails,
        updateRoles:updateRoles,
        blockElement:blockElement,
        hideElement:hideElement,
        updateCustomerInfo:updateCustomerInfo,
        updateTransactionInfo:updateTransactionInfo
    };
})();
