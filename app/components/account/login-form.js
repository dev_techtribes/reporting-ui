var loginForm = (function () {
    var initialiseEvents = function () {
        $('#forgot-password').click(function () {
            $('#modal-login').removeClass('open');
            $('#modal-recovery').addClass('open');
        });

        $('#back-to-login').click(function () {
            $('#modal-recovery').removeClass('open');
            $('#modal-login').addClass('open');
        });

        $('#submit-recovery').click(function () {
            $('#modal-recovery').removeClass('open');
            $('#modal-recovery-feedback').addClass('open');
        });

        $('#recovery-done').click(function () {
            $('#modal-recovery-feedback').removeClass('open');
            $('#modal-login').addClass('open');
        });
        $(".cp-btn").click(function () {
            $("#cp-result").hide();
            if (validateForm($('#cp-form'))) {
                accountService.changePassword({
                    userId: $('#email').val(),
                    oldPassword: $('#password').val(),
                    password: $('#pswd').val()
                }).then(function (data) {
                    if (data.status == 'SUCCESS') {
                        router.page('transaction');
                    }
                    else {
                        $("#cp-result").show();
                    }
                }, function (data) {
                    $("#cp-result").show();
                    //router.page('transaction');
                });
            }
        });
        $(".login-btn").click(function (event) {
            event.preventDefault();
            $("#result").hide();
            if (validateForm($('#login-form'))) {

                accountService.login({
                    userId: $('#email').val(),
                    password: $('#password').val()
                }).then(function (data) {
                    if (data.errorCode == env.ipBLock)
                        router.page('ipblock');
                    else if (data.status == 'SUCCESS') {
                        accountService.validateUser().then(function (data) {
                            if (data.status == 'SUCCESS' || data.status == null) {
                                env.agentName = data.lastName + " " + data.firstName;
                                $('#backOfficeAgentName').text(env.agentName);
                                accountService.setUserData(data);
                            //     rbac.getRbacDetails().then(function (data) {
                                     router.page('transaction');
                            //    });
                           }
                        });
                        if (data.lastLoginDate == null) {
                            $('#modal-login').removeClass('open');
                            $('#modal-cp').addClass('open');

                        }
                        /*else{
                           router.page('transaction');
                        }*/
                    }
                    else {
                        $("#result").show();
                        $("#modal-login input.form-control").addClass("error");
                    }

                }, function (data) {
                    $("#result").show();
                    $("#modal-login input.form-control").addClass("error");
                    //router.page('transaction');
                    // $('#modal-login').removeClass('open');
                    //     $('#modal-cp').addClass('open');
                });
            }
        });
    }
    var init = function () {
        initialiseEvents();
        validations.init();
        $("#result").hide();
        $("#cp-result").hide();
        $('#modal-login').addClass('open');
        $('.login').css('background', '#fff url(images/cortex-background-animated.gif) no-repeat');
    }
    return {
        init: init
    }
})();
