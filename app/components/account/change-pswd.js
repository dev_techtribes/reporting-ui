var changePassword = (function () {
    var initialiseEvents = function () {
        $('#change-password').click(function () {


            $('#change-password-modal').modal('show');
             
            resetValidation($('#cp-form'));
             $("#cp-result").hide();

            $('#oldPswd').val("");
            $('#pswd').val("");
            $('#cpswd').val("");
        });

        $(".cp-btn").click(function () {
            $("#cp-result").hide();
            if (validateForm($('#cp-form'))) {
                    accountService.changePassword({
                    userId: user.userid,
                    oldPassword: $('#oldPswd').val(),
                    password: $('#pswd').val()
                }).then(function (data) {
                    if (data.status == 'SUCCESS') {
                        alert('Password changed Successfully.');
                        $('#change-password-modal').modal('toggle');
                    }else if(data.errorCode == "INVALID_PASSWORD"){
                        $('#oldPswd').removeClass('error');
                        $('#oldPswd').removeClass("validation-error");
                        $('#oldPswd').next().remove();
                        $('<label class="error">Invalid Password</label>').insertAfter($('#oldPswd'));
                    }
                    else {
                        $("#cp-result").show();
                    }
                }, function (data) {
                    $("#cp-result").show();
                    //router.page('transaction');
                });
            }
        });


    }



    var init = function () {
        initialiseEvents();
       // validations.init();
        $("#result").hide();
        $("#cp-result").hide();

    }
    return {
        init: init
    }

})();
