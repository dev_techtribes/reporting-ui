var searchBox = (function () {
    var backup_datarows = "";
    var populateSearchData = function (data, searchData) {
        if (data != null) {
            data.map(function (tx) {
                if (!searchData.customerId.hasOwnProperty(tx.transactionInfo.merchantCustomerId)) {
                    searchData.customerId[tx.transactionInfo.merchantCustomerId] = '';
                }
                if (!searchData.method.hasOwnProperty(tx.transactionInfo.cardType)) {
                    searchData.method[tx.transactionInfo.cardType] = '';
                }
                if (!searchData.dalberryRef.hasOwnProperty(tx.transactionInfo.dalberryTxnReference)) {
                    searchData.dalberryRef[tx.transactionInfo.dalberryTxnReference] = '';
                }
                if (!searchData.merchantTxnId.hasOwnProperty(tx.transactionInfo.merchantTxnId)) {
                    searchData.merchantTxnId[tx.transactionInfo.merchantTxnId] = '';
                }
                if (!searchData.ccy.hasOwnProperty(tx.transactionInfo.txnCurrency)) {
                    searchData.ccy[tx.transactionInfo.txnCurrency] = '';
                }
                if (!searchData.status.hasOwnProperty(tx.transactionInfo.txnStatus)) {
                    searchData.status[tx.transactionInfo.txnStatus] = '';
                }
                if (!searchData.txnType.hasOwnProperty(tx.transactionInfo.txnType)) {
                    searchData.txnType[tx.transactionInfo.txnType] = '';
                }
            });
        }
    };
    var formatData = function (searchData) {
        var refinedData = [];
        for (var cat in searchData) {
            if (searchData.hasOwnProperty(cat)) {
                for (var val in searchData[cat]) {
                    if (searchData[cat].hasOwnProperty(val)) {
                        var catName;
                        if (cat == 'customerId') {
                            catName = env.searchBoxLabelCustomerId;
                        }
                        else if (cat == 'method') {
                            catName = env.searchBoxLabelMethod;
                        }
                        else if (cat == 'dalberryRef') {
                            catName = env.searchBoxLabelDLBRef;
                        }
                        else if (cat == 'merchantTxnId') {
                            catName = env.searchBoxLabelMerchantTxnId;
                        }
                        else if (cat == 'status') {
                            catName = env.searchBoxLabelTxnStatus;
                        }
                        else if (cat == 'ccy') {
                            catName = env.searchBoxLabelCcy;
                        }
                        else if (cat == 'txnType') {
                            catName = env.searchBoxLabelTxnType;
                        }
                        var obj = {
                            label: val,
                            category: catName,
                            classname: 'category-1'
                        }
                        //+ ' (' + Object.keys(searchData[cat]).length + ' results)'
                        refinedData.push(obj);
                    }
                }
            }
        }
        return refinedData;
    };
    var showAll = function (e) {
        $(e).addClass('hide');
        ele = $(e).prev();
        while (ele.hasClass('hide')) {
            ele.removeClass('hide');
            ele = ele.prev();
        }
    };
    var elementSelected = function (e) {
        var data = {};
        data[e.dataset.category] = e.dataset.label;
        $('.search-box .search-form input').val(e.dataset.label);
        $('.ui-menu').css('display', 'none');
        transactionTable.filterDataTable(data)
    };
    var declareWidget = function () {
        $.widget("custom.catcomplete", $.ui.autocomplete, {
            options: {
                highlightClass: "ui-state-highlight",
                messages: {
                    noResults: '',
                    results: function () { }
                }
            },
            _create: function () {
                this._super();
                this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
            },
            _renderItem: function (ul, item) {
                var re = new RegExp("(" + this.term + ")", "gi"),
                    cls = this.options.highlightClass,
                    template = "<mark class='" + cls + "'>$1</mark>",
                    label = item.label.replace(re, template),
                    $li = $("<li/>").appendTo(ul);

                // Create and return the custom menu item content.
                $("<a/>").html(label).appendTo($li);
                return $li;
            },
            _renderMenu: function (ul, items) {
                var that = this,
                    currentCategory = "";
                var count = 0;
                var max = 3;
                console.log(items.length);
                $.each(items, function (index, item) {
                    var li;
                    if (item.category != currentCategory) {
                        var lastCategory = ul.find(".ui-autocomplete-category").last();
                        if (lastCategory) {
                            lastCategory.text(lastCategory.text() + ' (' + count + ' results)');
                        }
                        ul.append("<li class='ui-autocomplete-category " + item.classname + "'>" + item.category + "</li>");
                        currentCategory = item.category;
                        count = 0;
                    }
                    li = that._renderItemData(ul, item);
                    count++;
                    if (item.category) {
                        li.attr("aria-label", item.category + " : " + item.label)
                            .attr("data-label", item.label)
                            .attr("data-category", item.category)
                            .attr("onclick", 'searchBox.elementSelected(this)').addClass(item.classname);
                        if (count >= max)
                            li.addClass('hide');
                    }
                });
                var lastCategory = ul.find(".ui-autocomplete-category").last();
                if (lastCategory) {
                    lastCategory.text(lastCategory.text() + ' (' + count + ' results)');
                }
            }
        });

    };
    var configureWidget = function () {

        $('.search-form .form-control').find('input').autocomplete({
            source: data,
            close: function( event, ui ) {
                $('.nicescroll-rails').css("display", "none");
            },
            messages: {
                noResults: '',
                results: function () { }
            }
        });

        var data = [];
        $(".search-form .form-control").catcomplete({
            delay: 0,
            source: data,
            close: function( event, ui ) {
                $('.nicescroll-rails').css("display", "none");
            },
            messages: {
                noResults: '',
                results: function () { }
            },
            appendTo: ".search-collapse",
            open: function (event, ui) {

                $('.ui-autocomplete').niceScroll({
                    scrollspeed: 200,
                    mousescrollstep: 40,
                    autohidemode: false,
                    cursorwidth: 3,
                    cursorcolor: "#999",
                    touchbehavior: true,
                    grabcursorenabled: false,
                    cursorborder: "0",
                    railpadding: {
                        top: 5,
                        right: 2,
                        left: 0,
                        bottom: 5
                    },
                    cursorborderradius: "2px"
                });
                // console.log(items.length);
                // if (items.length > 0)
                //     $('.nicescroll-rails').css("display", "block");
                // else
                //     $('.nicescroll-rails').css("display", "none");
                $('.ui-autocomplete li').each(function () {
                    if ($(this).next().hasClass('ui-autocomplete-category') && $(this).hasClass('hide')) {
                        $(this).after('<li class="show-all" onclick="searchBox.showAll(this)"><a>Show all results</a></li>');
                    }
                    if ($(this).is(':last-child') && $(this).hasClass('hide')) {
                        $(this).after('<li class="show-all" onclick="searchBox.showAll(this)"><a>Show all results</a></li>');
                    }
                });
                $(".dashboard .ui-autocomplete > li.ui-menu-item").removeClass('category-1 ui-menu-item').css("margin-left", "27px");
                $('.nicescroll-rails').click(function (e) {
                    e.stopPropagation();
                });
            }
        });


    }
    var prepareSearchList = function (data) {
        if (env.searchBoxInitialise == undefined) {
            // $('.search-box .search-form input').off();
            searchBox.init();
            env.searchBoxInitialise = true;
        }
        var searchData = {
            "customerId": {},
            "method": {},
            "dalberryRef": {},
            "merchantTxnId": {},
            "ccy": {},
            "status": {},
            "txnType": {}
        };
        populateSearchData(data, searchData);
        var refinedData = formatData(searchData);
        console.log(refinedData);
        $(".search-form .form-control").catcomplete('option', 'source', refinedData);
    };
    var initialiseEvents = function () {
        $('.search-box .search-opener').click(function (e) {

            closeUserMenu(e);

            $(this).closest('.search-box').toggleClass('opened');
            $(this).closest('.search-box').find('.search-collapse').toggleClass('closed');

            //$('.search-box .search-form input').val('');

            if ($(this).closest('.search-box').hasClass('opened') == false) {

                transactionTable.resetTable(backup_datarows);
                $('.search-box .search-form input').val('');
                backup_datarows = "";
            }
            else {

                backup_datarows = $('.transactions-table tbody').html();

            }

            calcSearchWidth();
            return false;
        });
    };
    var init = function () {
        $('.search-box .search-form input').keyup(function () {
            $('.nicescroll-rails').css("display", "none");
        });
        declareWidget();
        configureWidget();
        initialiseEvents();
    };
    return {
        init: init,
        showAll: showAll,
        prepareSearchList: prepareSearchList,
        elementSelected: elementSelected
    }

    function closeUserMenu(e) {
        //CLOSE USER MENU
        var userMenu = $('.dropdown');
        if (null != userMenu && undefined != userMenu && !userMenu.is(e.target)) {
            $('.dropdown').removeClass('active');
            userMenu.find('.dropdown-menu').fadeOut();
        }
    }
})();
