var exportCsv = (function () {

    var init = function(){
        eventListeners();
    };

    var collectVisibleHeaders =  function () {

        var visibleHeaders = $('.transactions-table th:visible')
        var visibleHeaderList = [];
        visibleHeaders.each(function(){
            var headerName = $(this).data("header-name");
            if(headerName)
            {
                visibleHeaderList.push(headerName);
            }

        });

        return visibleHeaderList;
    }

    var eventListeners = function () {

        $("body").on('click', '#exportToCSV' , function(e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            var visibleHeaderList = collectVisibleHeaders();
            console.log(visibleHeaderList);
            var inData =  $.extend(true, {}, env.getTransactionsRequestObject);
            inData.index = 0;
            inData.size = env.tempCollection.count+1;
            inData.fieldsToBeVisible = visibleHeaderList.slice();
            txAction.exportTablecsv(inData).then(function(data){
                var csvAsDataUri = 'data:application/csv,'+data;
                $('body').append('<a id="csvReport" ></a>');
                var blob = new Blob([data],{type: "text/csv;charset=utf-8;"});
                if (navigator.msSaveBlob) { // IE 10+
                navigator.msSaveBlob(blob, "Report.csv")
                } else {
                $('#csvReport').attr('href',encodeURI(csvAsDataUri));
                $('#csvReport').attr('download',"Report.csv");
                $("#csvReport")[0].click();
            }
            });


        });
    }


    return {
        init : init
    }
})()
