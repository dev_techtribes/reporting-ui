var sideBar = (function () {
    var init = function () {
        $('.menu-opener').mouseenter(function () {
            if($(this).hasClass('opened')){
                $(this).addClass("change");
                $(this).removeClass("black"); 
            }
            else{
                $(this).removeClass("change");
                $(this).addClass("black");
            }                   
        });
        $('.menu-opener').mouseleave(function () {
            $(this).removeClass("change");
            $(this).removeClass("black");
        });
        $('#trans-info-apply').click(function (e) {
            e.stopPropagation();
            $('form[name=txInfo] li input ').each(function () {
                if (this.checked) {
                    var name = this.value;
                    var currentId = '.' + this.value;
                    console.log(currentId);
                    env.optionalHeaderListTxInfo = jQuery.grep(env.optionalHeaderListTxInfo, function (value) {
                        return value != name;
                    });
                    $(currentId).show();
                    $(currentId).removeClass('hide');


                }
                else {
                    var currentId = '.' + this.value;
                    console.log(currentId);
                    env.optionalHeaderListTxInfo.push(this.value);
                    $(currentId).hide();
                    $(currentId).addClass('hide');
                }
            })
            hideSideBar();

        });

        $('#cust-info-apply').click(function () {
            $('form[name=custInfo] li input ').each(function () {
                if (this.checked) {
                    var name = this.value;
                    var currentId = '.' + this.value;
                    console.log(currentId);

                    env.optionalHeaderListCustInfo = jQuery.grep(env.optionalHeaderListCustInfo, function (value) {
                        return value != name;
                    });
                    $(currentId).show();
                    $(currentId).removeClass('hide');

                }
                else {
                    var currentId = '.' + this.value;
                    console.log(currentId);
                    env.optionalHeaderListCustInfo.push(this.value);
                    $(currentId).hide();
                    $(currentId).addClass('hide');
                }
            });
            hideSideBar();
        });
        $('#sidebar').removeClass('sidebar-opened');
        $('#sidebar').css({ "transform": "perspective(300px) rotateY(0deg)" })
    };

    var hideSideBar = function () {
        var TIMEOUT = 300;
        $('.menu-opener').removeClass('opened');
        $('.overlay').fadeOut(TIMEOUT);
        $('html').removeClass('opening');
        setTimeout(function () {
            $('html').removeClass('push');
        }, TIMEOUT);
        transactionTable.markLastColumn();
    };

    return {
        init: init,
        hideSideBar:hideSideBar
    }
})();
