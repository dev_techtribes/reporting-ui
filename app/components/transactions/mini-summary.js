var miniSummary = (function () {

    function isSignRequired(amount, symbol) {
        if(amount > 0)
            return '<td>' + symbol + ' ';
        else
            return '<td>';
    }

    function sortData(data) {
        data.sort(function(a, b) {
            var nameA = a.currency.toUpperCase(); // ignore upper and lowercase
            var nameB = b.currency.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
            return -1;
            }
            if (nameA > nameB) {
            return 1;
            }
            return 0;
        });

        Array.prototype.move = function(from,to){
            this.splice(to,0,this.splice(from,1)[0]);
            return this;
        };

        for (var i = 0; i < data.length; i++) {
            console.log(data[i].currency.toUpperCase);
            if(data[i].currency.toUpperCase() == "EUR"){
                data.move(i,0);
            }

            if(data[i].currency.toUpperCase() == "USD"){
                if(data[0].currency.toUpperCase() == "EUR") {
                    data.move(i,1);
                } else {
                    data.move(i,0);
                }
            }

            if (data[i].currency.toUpperCase() == "GBP") {
                if(data[0].currency.toUpperCase() == "EUR") {
                  if(data[1].currency.toUpperCase() == "USD") {
                      data.move(i,2);
                  } else {
                      data.move(i,1);
                  }
                } else if(data[0].currency.toUpperCase() == "USD") {
                      data.move(i,1);
                } else {
                    data.move(i,0);
                }
            }
        }

        return data;
    }

    Array.prototype.move = function(from,to){
            console.log("reached move");
            this.splice(to,0,this.splice(from,1)[0]);
            return this;
    };


    var init = function (data) {
        data = sortData(data);
        var summary_html = "";
        if (data != null) {
            console.log("data length :: "+data.length)


            for (var i = 0; i < data.length; i++) {
                var revenue = 0;
                if(data[i].revenue != null){
                    revenue = data[i].revenue.toFixed(2);
                }
                summary_html = summary_html + '<tr>' +
                    '<td>' + data[i].currency + '</td>' +
                    /*isSignRequired(data[i].incomingAmount.toFixed(2),'+') + data[i].incomingAmount.toFixed(2) + '</td>' +
                    '<td>' + data[i].incomingTransactionCount + '</td>' +
                    isSignRequired(data[i].outgoingAmount.toFixed(2),'-') + data[i].outgoingAmount.toFixed(2) + '</td>' +
                    '<td>' + data[i].outgoingTransactionCount + '</td>' +*/
                    '<td>' + formatAmounts(revenue) + '</td>' +
                    '<td>' + data[i].totalTransactions + '</td>' +
                    '</tr>';

            }
        }
        $(".info-summary table tr:gt(0)").remove();
        $(".info-summary table tbody").html(summary_html);
        var length = $('.info-summary table th').length,
            cont;

        for (var i = 0; i < length; i++) {
            cont = $('.info-summary table th').eq(i).wrapInner('<div class="th-xs"></div>').html();

            $('.info-summary table tr').each(function() {
                $(this).find('.cell').eq(i).prepend(cont);
            });
        }
        initialiseEvents();
        if(data.length > 1){
                console.log("reached dragon")
                $('.button-arrow').removeClass('fa fa-angle-down active');
                $('.button-arrow').addClass('fa fa-angle-up');
                // $('.summary-box .button-arrow').trigger('click');
                // $('.summary-box .button-arrow').siblings('table').find('tr:nth-child(n+2) > td > .cell').toggle();
        } else {
            $('.info-summary table td .cell').css("padding-bottom", 0);
            $('.button-arrow').removeClass('fa fa-angle-up fa-angle-down');
        }
    };
    var initialiseEvents = function(){
       /* $(document).on('click', function (e) {
            var miniSummary = $('.summary-box');
            console.log('ajksdh');
            if(!miniSummary.is(e.target.closest(".summary-box"))) {
               $(".summary-box").removeClass("open");
            }
        });*/

        $('.summary-box table').find('td').wrapInner('<div class="cell"></div>');
        $('.summary-box .button-arrow').unbind("click").click(function(e){
            console.log("minisummaryclicked");
            $(this).siblings('table').find('tr:nth-child(n+2) > td > .cell').slideToggle();
            $(this).toggleClass('active');

            e.stopPropagation();
        });




    };

    var resetMiniSummary = function(){
        var summary_html = "";
        console.log("resetmini");
        $(".info-summary table tbody").html(summary_html);

    }

    return {
        init: init,
        initialiseEvents:initialiseEvents,
        resetMiniSummary :  resetMiniSummary
    }


})()
