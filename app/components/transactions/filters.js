var filters = (function () {


    var jsonSort = function (arr, prop) {

        arr.sort(function (a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        });

    }

    var init = function () {
        initialiseEvents();
    }

    var setDropDownHeight=function(){
        if($('.dlb-table-cover').height()-$(window).scrollTop()-180 > 300)
            $('.option-filter-box .check-group').css('max-height','300px');
        else
            $('.option-filter-box .check-group').css('max-height',($('.dlb-table-cover').height()-$(window).scrollTop()-180)+'px');
    };

    var applyFilters=function(element)
    {

            var customerId = $('#customerid-val').val();
            var amountFrom = $('#amount-from').val();
            var amountTo = $('#amount-to').val();
            var dlbrefId = $('#dlbtxnid-val').val();
            var merchanttxnId = $('#merchanttxnid-val').val();
            var parentdiv = $(element).closest(".table-sort");
            var currentFilterType = parentdiv.find(".drop-opener").data("filter-type");
            env.activeFilterList[currentFilterType] = true;
            var currencyList = [];


                $('ul.currency-container input:checked').each(function () {
                    if ($(this).val() != "all") {
                        currencyList.push($(this).val());
                    }
                });





            var statusList = [];

                $('ul.status-container input:checked').each(function () {
                    if ($(this).val() != "all") {
                        statusList.push($(this).val());
                    }

                });



            var methodList = [];

                $('ul.methods-container input:checked').each(function () {
                    if ($(this).val() != "all") {
                        methodList.push($(this).val());
                    }

                });



            var typeList = [];

                $('ul.txntype-container input:checked').each(function () {
                    if ($(this).val() != "all") {
                        typeList.push($(this).val());
                    }
                });


            var merchantList = [];

                $('ul.merchantDetails-container input:checked').each(function () {
                    if ($(this).val() != "all") {
                        merchantList.push($(this).val());
                    }

                });


            var midList = [];

                $('ul.midDetails-container input:checked').each(function () {
                    if ($(this).val() != "all") {
                        midList.push($(this).val());
                    }
                });



            var profileList = [];

                $('ul.brandDetails-container input:checked').each(function () {
                    if ($(this).val() != "all") {
                        profileList.push($(this).val());
                    }

                });


            /*
             *for -ve amounts only;
             *
             */
            if (parseFloat(amountFrom) < 0 && parseFloat(amountTo) < 0 && parseFloat(amountFrom) < parseFloat(amountTo)) {
                var tempAmount = amountFrom;
                amountFrom = amountTo;
                amountTo = tempAmount;
            }

            env.getTransactionsRequestObject.currencyList = currencyList.slice();
            env.getTransactionsRequestObject.merchantList = merchantList.slice();
            env.getTransactionsRequestObject.profileList = profileList.slice();
            env.getTransactionsRequestObject.midList = midList.slice();
            env.getTransactionsRequestObject.methodList = methodList.slice();
            env.getTransactionsRequestObject.txnStatusList = statusList.slice();
            env.getTransactionsRequestObject.dalberryTxnReference = dlbrefId;
            env.getTransactionsRequestObject.merchantTxnId = merchanttxnId;
            env.getTransactionsRequestObject.maxTxnAmount = amountTo;
            env.getTransactionsRequestObject.minTxnAmount = amountFrom;
            env.getTransactionsRequestObject.merchantCustomerId = customerId;
            env.getTransactionsRequestObject.txnTypeList = typeList.slice();
            env.updatedByPagination=false;
            env.getTransactionsRequestObject.index=1;
            env.updatedByQuickSummary=1;
            transactionTable.getTransactionInfo();

            //$(".transactions-table-container").css("overflow-x","scroll");
            $(".transactions-table th").removeClass("blue");
            $('.table-dropdown').fadeOut();

        };

    var initialiseEvents = function () {
        $('.transactions-table th .table-sort').on("click", function (event) {
            if ($(event.target).is('.drop-opener, .table-dropdown *')) {
                event.stopImmediatePropagation();
            }
        });

        $('.table-sort').on('click', "span.drop-opener", function (event) {
            setDropDownHeight();
            event.stopPropagation();
           event.preventDefault();
                    var ul = $("#change-password").parent().parent();
                    ul.fadeOut();
                $this = $(this);

                $('.check-group li').css('display', 'block');

                var filtername = $(this).data("filter-type");


                if ($(this).closest('th').find('.table-dropdown').length) {

                    $("ul.check-group").scrollTop("0");
                    $("ul.check-group").scrollLeft(0);
                    $("span.drop-opener").each(function(element){
                        var current = $(element).data("filter-type");
                        if(env.activeFilterList[current] === false || env.activeFilterList.hasOwnProperty(current) === false) {
                            $(element).removeClass('active');
                        }
                    });
                        $('.table-dropdown').hide();
                        $('.transactions-table-container').css("overflow","visible");
                        $this.addClass('active');
                        $this.closest('th').find('.table-dropdown').show();
                        $this.closest('th').find('.table-dropdown').animateCss('bounceInDown');
                        $('.table-dropdown').find(".check-group").scrollTop("0").scrollLeft("0");
                   /* } else {
                        /!*if(env.activeFilterList[filtername] === false ||  env.activeFilterList.hasOwnProperty(filtername) === false)
                        {
                            $this.removeClass('active');
                        }
                        $('.transactions-table-container').css("overflow","auto");
                        $this.closest('th').find('.table-dropdown').fadeOut();*!/

                    }
*/

                    var currentFilterClass = $(this).closest('th').find('.table-dropdown').find(".option-filter-box");

                    if (filtername == "method" && env.getMethodMasterData.length == 0) {
                        if ($this.hasClass('active')) {
                            masterService.getPaymentInstruments().then(function (data) {
                                env.getMethodMasterData = data.slice();
                                jsonSort(data, 'paymentInstrument');
                                var index = 0;
                                var checkBox_html = '<li>' +
                                    '<div class="checkbox"><input type="checkbox"  class="all invisible" id="lbl-' + currentFilterClass.attr("class") + '-all" value="all" checked="checked"><div class="custom-check"></div></div>' +
                                    '<label for="lbl-01">All</label>' +
                                    '</li>';

                                for (index = 0; index < data.length; index++) {
                                    checkBox_html = checkBox_html + '<li>' +
                                        '<div class="checkbox"><input type="checkbox" class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].id + '" checked="checked"><div class="custom-check"></div></div>' +
                                        '<label for="lbl-02">' + data[index].paymentInstrument + '</label>' +
                                        '</li>';
                                }

                                $("ul.methods-container").html(checkBox_html);

                            });

                        }
                    }




                    if (filtername == "txType" && env.getTypeMasterData.length==0) {
                        if ($this.hasClass('active')) {
                            // if(env.getTypeMasterData.length>0 && env.getTransactionsRequestObject.txnType){
                            //     var checkBox_html = '<li>' +
                            //         '<div class="checkbox"><input type="checkbox"  class="all invisible" id="lbl-' + currentFilterClass.attr("class") + '-all" value="all"><div class="custom-check"></div></div>' +
                            //         '<label for="lbl-01">All</label>' +
                            //         '</li>';

                            //         for (index = 0; index < data.length; index++) {
                            //             console.log("data[index].type :: "+data[index].type);
                            //             if(env.getTransactionsRequestObject.txnType === data[index].type)
                            //             {
                            //                  checkBox_html = checkBox_html + '<li>' +
                            //                 '<div class="checkbox"><input type="checkbox"  class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].id + '" checked="checked"><div class="custom-check"></div></div>' +
                            //                 '<label for="lbl-02">' + data[index].type + '</label>' +
                            //                 '</li>';
                            //             }
                            //             else
                            //             {
                            //                 checkBox_html = checkBox_html + '<li>' +
                            //                 '<div class="checkbox"><input type="checkbox" class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].id + '"><div class="custom-check"></div></div>' +
                            //                 '<label for="lbl-02">' + data[index].type + '</label>' +
                            //                 '</li>';
                            //             }
                            //             $("ul.txntype-container").html(checkBox_html);
                            //             return;
                            //         }
                            // }

                            masterService.getTransactionTypes().then(function (data) {
                                //data.push({ id: 0, type: "Sale" })
                                env.getTypeMasterData = data.slice();
                                jsonSort(data, 'type');
                                var index = 0;
                                console.log("env.getTransactionsRequestObject.txnType "+env.getTransactionsRequestObject.txnType);
                                if(env.getTransactionsRequestObject.txnType == null){
                                    var checkBox_html = '<li>' +
                                        '<div class="checkbox"><input type="checkbox"  class="all invisible" id="lbl-' + currentFilterClass.attr("class") + '-all" value="all" checked="checked"><div class="custom-check"></div></div>' +
                                        '<label for="lbl-01">All</label>' +
                                        '</li>';

                                    for (index = 0; index < data.length; index++) {
                                        checkBox_html = checkBox_html + '<li>' +
                                            '<div class="checkbox"><input type="checkbox"  class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].id + '" checked="checked"><div class="custom-check"></div></div>' +
                                            '<label for="lbl-02">' + data[index].type + '</label>' +
                                            '</li>';
                                    }
                                }

                                $("ul.txntype-container").html(checkBox_html);
                            });

                        }
                    }

                    if (filtername == "currency" && env.getCurrencyMasterData.length == 0) {

                        if ($this.hasClass('active')) {
                            masterService.getCurrencies().then(function (data) {
                                env.getCurrencyMasterData = data.slice();
                                jsonSort(data, 'code');
                                var index = 0;
                                var checkBox_html = '<li>' +
                                    '<div class="checkbox"><input type="checkbox"   class="all invisible" id="lbl-' + currentFilterClass.attr("class") + '-all" value="all" checked="checked"><div class="custom-check"></div></div>' +
                                    '<label for="lbl-01">All</label>' +
                                    '</li>';

                                for (index = 0; index < data.length; index++) {
                                    checkBox_html = checkBox_html + '<li>' +
                                        '<div class="checkbox"><input type="checkbox" class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].id + '" checked="checked"><div class="custom-check"></div></div>' +
                                        '<label for="lbl-02">' + data[index].code + '</label>' +
                                        '</li>';

                                }

                                $("ul.currency-container").html(checkBox_html);
                            });

                        }
                    }
                    if (filtername == "txstatus" && env.getStatusMasterData.length == 0) {
                        if ($this.hasClass('active')) {

                            masterService.getTransactionStatus().then(function (data) {
                                env.getStatusMasterData = data.slice();

                                jsonSort(data, 'status');

                                var index = 0;
                                var checkBox_html = '<li>' +
                                    '<div class="checkbox"><input type="checkbox"   class="all invisible" id="lbl-01" value="all" checked="checked"><div class="custom-check"></div></div>' +
                                    '<label for="lbl-01">All</label>' +
                                    '</li>';

                                for (index = 0; index < data.length; index++) {
                                    checkBox_html = checkBox_html + '<li>' +
                                        '<div class="checkbox"><input type="checkbox" class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].id + '" checked="checked"><div class="custom-check"></div></div>' +
                                        '<label for="lbl-02">' + data[index].status + '</label>' +
                                        '</li>';

                                }

                                $("ul.status-container").html(checkBox_html);
                            });

                        }
                    }
                    if (filtername == "merchantDetails" && env.getMerchantMasterData.length == 0) {
                        if ($this.hasClass('active')) {

                            masterService.getMerchants().then(function (data) {
                                env.getMerchantMasterData = data.slice();

                                jsonSort(data, 'merchant');

                                var index = 0;
                                var checkBox_html = '<li>' +
                                    '<div class="checkbox"><input type="checkbox"   class="all invisible" id="lbl-01" value="all" checked="checked"><div class="custom-check"></div></div>' +
                                    '<label for="lbl-01">All</label>' +
                                    '</li>';

                                for (index = 0; index < data.length; index++) {
                                    checkBox_html = checkBox_html + '<li>' +
                                        '<div class="checkbox"><input type="checkbox" class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].id + '" checked="checked"><div class="custom-check"></div></div>' +
                                        '<label for="lbl-02">' + data[index].merchant + '</label>' +
                                        '</li>';

                                }

                                $("ul.merchantDetails-container").html(checkBox_html);
                            });

                        }
                    }

                    if (filtername == "midDetails" && env.getMidMasterData.length == 0) {
                        if ($this.hasClass('active')) {

                            masterService.getMids().then(function (data) {
                                env.getMidMasterData = data.slice();

                                jsonSort(data, 'processor');

                                var index = 0;
                                var checkBox_html = '<li>' +
                                    '<div class="checkbox"><input type="checkbox"   class="all invisible" id="lbl-01" value="all" checked="checked"><div class="custom-check"></div></div>' +
                                    '<label for="lbl-01">All</label>' +
                                    '</li>';

                                for (index = 0; index < data.length; index++) {
                                    checkBox_html = checkBox_html + '<li>' +
                                        '<div class="checkbox"><input type="checkbox" class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].processorId + '" checked="checked"><div class="custom-check"></div></div>' +
                                        '<label for="lbl-02">' + data[index].processor + '</label>' +
                                        '</li>';

                                }

                                $("ul.midDetails-container").html(checkBox_html);
                            });

                        }
                    }

                    if (filtername == "brandDetails" && env.getBrandMasterData.length == 0) {
                        if ($this.hasClass('active')) {

                            masterService.getProfiles().then(function (data) {
                                env.getBrandMasterData = data.slice();

                                jsonSort(data, 'profile');

                                var index = 0;
                                var checkBox_html = '<li>' +
                                    '<div class="checkbox"><input type="checkbox"   class="all invisible" id="lbl-01" value="all" checked="checked"><div class="custom-check"></div></div>' +
                                    '<label for="lbl-01">All</label>' +
                                    '</li>';

                                for (index = 0; index < data.length; index++) {
                                    checkBox_html = checkBox_html + '<li>' +
                                        '<div class="checkbox"><input type="checkbox" class="invisible"  id="lbl-' + currentFilterClass.attr("class") + index + '" value="' + data[index].id + '" checked="checked"><div class="custom-check"></div></div>' +
                                        '<label for="lbl-02">' + data[index].profile + '</label>' +
                                        '</li>';

                                }

                                $("ul.brandDetails-container").html(checkBox_html);
                            });

                        }
                    }

                }

       });

        $(".table-dropdown").find(".search-form").on("keyup", "input", function () {
            console.log("pressed");
            var query = this.value;
            $('.check-group [id^="lbl"]').each(function (i, elem) {

                if ($(this).closest('li').find("label").text().toLowerCase().indexOf(query.toLowerCase()) != -1) {
                    $(this).closest('li').show();
                }
                else {
                    $(this).closest('li').hide();
                }
            });
        });

        $(".btns-row").on("click", ".clear-dates", function () {

            var startDate = moment().subtract(24, 'hours');
            var endDate = moment();
            env.getTransactionsRequestObject.startDate = moment().subtract(24, 'hours').format("YYYY-MM-DD HH:mm:ss");
            env.getTransactionsRequestObject.endDate = moment().format("YYYY-MM-DD HH:mm:ss");
            $('.date-range input').val(startDate.format('DD-MMM-YYYY') + ' - ' + endDate.format('DD-MMM-YYYY'));
            $('.dates-list li').removeClass('liSelect');
            $('.dates-list li:first-child').addClass('liSelect');
            var parent =  $(this).closest(".table-sort");
            var filterType = parent.find(".drop-opener").data("filter-type");
            env.activeFilterList[filterType] = false;
            env.updatedByPagination=false;
            env.updatedByQuickSummary = 1;
            env.getTransactionsRequestObject.index=1;
            parent.find(".drop-opener").removeClass('active');
            transactionTable.getTransactionInfo();
            $(this).closest('.table-dropdown').fadeOut();
            $('.transactions-table-container').css("overflow","auto");
        });

        $(".btns-row").on("click", ".clear-filters", function () {
            var startDate = moment().subtract(24, 'hours');
            var endDate = moment();

            if (this.id == 'txnDate') {
                $('.date-range input').val(startDate.format('DD-MMM-YYYY') + ' - ' + endDate.format('DD-MMM-YYYY'));
                env.getTransactionsRequestObject.startDate = moment().subtract(24, 'hours').format("YYYY-MM-DD HH:mm:ss");
                env.getTransactionsRequestObject.endDate = moment().format("YYYY-MM-DD HH:mm:ss");
            }
            if (this.id == 'customerId') {
                $('#customerid-val').val("");
                env.getTransactionsRequestObject.merchantCustomerId = null;
            }
            if (this.id == 'amt') {
                $('#amount-from').val("");
                $('#amount-to').val("");
                $('.amt-error').text('');
                env.getTransactionsRequestObject.maxTxnAmount = null;
                env.getTransactionsRequestObject.minTxnAmount = null;
            }
            if (this.id == 'ref') {
                $('#dlbtxnid-val').val("");
                env.getTransactionsRequestObject.dalberryTxnReference = null;
            }
            if (this.id == 'mTxnId') {
                $('#merchanttxnid-val').val("");
                env.getTransactionsRequestObject.merchantTxnId = null;
            }
            if (this.id == 'ccy') {
                $('#currency-value').val("");
                env.getTransactionsRequestObject.currencyList = null;
            }
            if (this.id == 'status') {
                $('#status-value').val("");
                env.getTransactionsRequestObject.txnStatusList = null;
            }
            if (this.id == 'method') {
                $('#method-value').val("");
                env.getTransactionsRequestObject.methodList = null;
            }
            if (this.id == 'txnType') {
                $('#type-value').val("");

                env.getTransactionsRequestObject.txnType = null;

                env.getTransactionsRequestObject.txnTypeList = null;
            }
            if (this.id == 'merchantDetailsBtn') {
                $('#merchantDetails-value').val("");
                env.getTransactionsRequestObject.merchantList = null;
            }
            if (this.id == 'midDetailsBtn') {
                $('#midDetails-value').val("");
                env.getTransactionsRequestObject.midList = null;
            }
            if (this.id == 'brandDetailsBtn') {
                $('#mbrandDetails-value').val("");
                env.getTransactionsRequestObject.profileList = null;
            }

            var checkboxes = $(this).parent().parent().find('ul.check-group').find("input:checkbox");
            //var checkboxes = $("ul.check-group").find("input:checkbox");
            checkboxes.prop('checked', true);

            $(this).next(".apply-filters").prop("disabled",false);
            var parent =  $(this).closest(".table-sort");
            var filterType = parent.find(".drop-opener").data("filter-type");
            env.activeFilterList[filterType] = false;
            env.updatedByPagination=false;
            env.updatedByQuickSummary=1;
            env.getTransactionsRequestObject.index=1;
            transactionTable.getTransactionInfo();
            $(this).closest('.table-dropdown').fadeOut();
            parent.find(".drop-opener").removeClass('active');
            $('.transactions-table-container').css("overflow","auto");
        });

        $(".option-filter-box").on('click','input[type=checkbox]:not(.all)',function(e){

            console.log(e);

            var parent = $(this).parent().parent().parent();

            if(parent.find('input[type=checkbox]:not(.all):checked').length == parent.find('input[type=checkbox]:not(.all)').length)
            {

                parent.find(".all").prop('checked', "checked");
            }
            else
            {
                console.log("unchecked");
                parent.find(".all").prop('checked', false)
                parent.find(".all").removeAttr('checked')
            }

            if(parent.find('input[type=checkbox]:not(.all):checked').length > 0)
            {


                parent.parent().find(".apply-filters").prop("disabled",false);
            }
            else
            {
                parent.parent().find(".apply-filters").prop("disabled",true);
            }
        });

        $(".option-filter-box").on('click','input.all',function(e){

            console.log(e);

            var parent = $(this).parent().parent().parent();
            var optionsform = parent.parent();
            if($(this).prop('checked')) {

                parent.find('input:not(.all)').prop('checked', true);

                optionsform.find(".apply-filters").removeAttr("disabled");
            }
            else
            {
                parent.find('input:not(.all)').prop('checked', false);
                optionsform.find(".apply-filters").attr("disabled","true");
            }
            /*if(parent.find('input[type=checkbox]:not(.all):checked').length == parent.find('input[type=checkbox]:not(.all)').length)
            {
                console.log("all checked");
                parent.find("input.all").prop('checked', true)
            }
            else
            {
                parent.find("input.all").prop('checked', false)
            }*/
        });


        $('.table-dropdown .btns-row').on('click', '.apply-filters', function () {
            applyFilters(this);
            var parent = $(this).closest(".table-dropdown");

                parent.find(".option-filter-search input[type=search].form-control").val("");

        });

    /*    $("ul.status-container .all").click(function () {

            $(this).find("input:checkbox").prop('checked', $(this).prop('checked'));

        });*/

    }

    var markAll = function (e) {
        console.log("markall");
        if (e.checked)
            $(e).parent().parent().parent().find('input:not(.all)').prop('checked', true);
        else
            $(e).parent().parent().parent().find('input:not(.all)').prop('checked', false);
    }

    var unmarkAll = function (e) {
        console.log("markall");
        $(e).parent().parent().parent().find('.all').prop('checked', false);
    }

    var resetAllFilters = function(){
        var startDate = moment().subtract(24, 'hours');
        var endDate = moment();

            env.getTransactionsRequestObject.startDate = moment().subtract(24, 'hours').format("YYYY-MM-DD HH:mm:ss");
            env.getTransactionsRequestObject.endDate = moment().format("YYYY-MM-DD HH:mm:ss");
            $('.date-range input').val(startDate.format('DD-MMM-YYYY') + ' - ' + endDate.format('DD-MMM-YYYY'));
            $('.dates-list li').removeClass('liSelect');
            $('.dates-list li:first-child').addClass('liSelect');

            $('.date-range input').val(startDate.format('DD-MMM-YYYY') + ' - ' + endDate.format('DD-MMM-YYYY'));
            env.getTransactionsRequestObject.startDate = moment().subtract(24, 'hours').format("YYYY-MM-DD HH:mm:ss");
            env.getTransactionsRequestObject.endDate = moment().format("YYYY-MM-DD HH:mm:ss");


            $('#customerid-val').val("");
            env.getTransactionsRequestObject.merchantCustomerId = null;

            $('#amount-from').val("");
            $('#amount-to').val("");
            $('.amt-error').text('');
            env.getTransactionsRequestObject.maxTxnAmount = null;
            env.getTransactionsRequestObject.minTxnAmount = null;


            $('#dlbtxnid-val').val("");
            env.getTransactionsRequestObject.dalberryTxnReference = null;


            $('#merchanttxnid-val').val("");
            env.getTransactionsRequestObject.merchantTxnId = null;


            $('#currency-value').val("");
            env.getTransactionsRequestObject.currencyList = null;


            $('#status-value').val("");
            env.getTransactionsRequestObject.txnStatusList = null;


            $('#method-value').val("");
            env.getTransactionsRequestObject.methodList = null;

            $('#type-value').val("");
            env.getTransactionsRequestObject.txnType = null;
             env.getTransactionsRequestObject.txnTypeList = null;

            $('#merchantDetails-value').val("");
            env.getTransactionsRequestObject.merchantList = null;

            $('#midDetails-value').val("");
            env.getTransactionsRequestObject.midList = null;

            $('#mbrandDetails-value').val("");
            env.getTransactionsRequestObject.profileList = null;

            var checkboxes = $('.transactions-table thead').find('ul.check-group').find("input:checkbox");
            //var checkboxes = $("ul.check-group").find("input:checkbox");
            checkboxes.prop('checked', true);
            $('#quick-search').val("");
            $('.transactions-table thead').find(".apply-filters").prop("disabled",false);


            var filterList = $('span .drop-opener').map(function(){return $(this).data("filter-type");}).get();
            $.each(filterList,function(){
                env.activeFilterList[this] =  false;
              });
            env.updatedByPagination=false;
            env.updatedByQuickSummary=1;
            env.getTransactionsRequestObject.index=1;
            $.each(env.optionalHeaderListTxInfoCloned, function () {
                var currentId = '.' + this;
                $(currentId).hide();
            });
            env.optionalHeaderListTxInfo = env.optionalHeaderListTxInfoCloned;
            $.each(env.optionalHeaderListCustInfoCloned, function () {
                var currentId = '.' + this;
                $(currentId).hide();
            });
            env.optionalHeaderListCustInfo = env.optionalHeaderListCustInfoCloned;
            $('form[name="txInfo"]').trigger("reset");
            $('form[name="custInfo"]').trigger("reset");
            transactionTable.getTransactionInfo();
            $('.transactions-table thead').find('.table-dropdown').fadeOut();
            $('.transactions-table thead').find(".drop-opener").removeClass('active');
            $('.transactions-table-container').css("overflow","auto");
    };

    return {
        init: init,
        markAll:markAll,
        unmarkAll:unmarkAll,
        resetAllFilters:resetAllFilters,
        applyFilters:applyFilters,
        setDropDownHeight:setDropDownHeight
    };
})();






