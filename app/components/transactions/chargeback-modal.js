"use strict";

var chargeback = function () {
    var txData = {};
    var createChargeBack = function createChargeBack() {

        $(".create-chb-error").hide();
        var d = txData.transactionInfo;
        var c = txData.customerInfo;
        var reasons = "";
        var currentDate = moment().format("L");
        $('.chargeback-error').text('');
        $('.chargeback-success').text('');
        $("#chbInitiatedDate").text(currentDate);
        $("#dlbInitialTxnReference").text(d.dalberryTxnReference);
        $("#providerInitialTxnID").text(d.providerTxnId);
        $("#initialTxnProcessedDate").text(d.txnProcessedDate);
        $("#initialTxnAuthCode").text(d.authCode);
        $("#chargebackAmount").text(d.txnAmount);
        $("#chargebackCurrency").text(d.txnCurrency);
        $("#cardHolderName").text(d.cardHolderName);
        $("#chargeBackMethod").text(d.cardType);

        $("#paymentInstrument").text(d.paymentInstrument);
        $(".create-chb-error").hide();


        reasons += '<option value="" selected="selected">Nothing selected</option>';


        txAction.getChargebackReasons(d.dalberryTxnReference).then(function(data){


            for(var i=0;i< data.length;i++){
                reasons = reasons + "<option value='"+data[i].trmId+"'>"+data[i].reason+"</option>";
            }

            $("#chargeBackReason").html(reasons);
           /* $('#chargeBackReason').selectpicker({container:"#modal-add-chargeback"});
                        $('#chargeBackReason').selectpicker('refresh');*/


        })

        $("#modal-add-chargeback").modal({backdrop: 'static', keyboard: false});
        /*$("#modal-add-chargeback .modal-content").niceScroll({
            scrollspeed: 200,
            mousescrollstep: 40,
            autohidemode: false,
            cursorwidth: 3,
            cursorcolor: "#999",
            touchbehavior: true,
            cursorborder: "0",
            railpadding: {
                top: 5,
                right: 3,
                left: 0,
                bottom: 5
            },
            cursorborderradius: "2px"
        });*/
    };



    var init = function init(txnData) {

        txData = txnData;
        createChargeBack();
        initialiseEvents();
    };

    var resetConfirmationModal = function resetConfirmationModal (){
        $('.chargeback-error').text("");
        $('.chargeback-success').text('');
    }

    var resetChargebackCreationModal = function resetChargebackCreationModal(){

        $(".create-chb-error").hide();
      /*  $('.chargeback-error').text('');
        $('.chargeback-success').text('');*/
        $("#chargeBackReason").val("");
        $("#chargebackCaseId").val("");
        $("#providerTxnId").val("");
    }

    var drawConfirmationModal = function drawConfirmationModal(){

        //$("#tx-details-modal").modal("hide");
        var d = txData.transactionInfo;
        var c = txData.customerInfo;
        var reasons = "";
        var currentDate = moment().format("L");
        $("#chargebackAmountConfirm").text(d.txnAmount);
        $("#chargebackCurrencyConfirm").text(d.txnCurrency);
        $("#cardHolderNameConfirm").text(d.cardHolderName);
        $("#paymentInstrumentConfirm").text(d.paymentInstrument);
        $("#chargebackReasonConfirm").text($("#chargeBackReason").children(":selected").text());
        $("#chargebackMethodConfirm").text(d.cardType);
        $("#chargebackProviderTxnIdConfirm").text($("#providerTxnId").val());
        $("#chargebackInitDateConfirm").text($("#chbInitiatedDate").text());
        $("#chargebackCaseIdConfirm").text($("#chargebackCaseId").val());
        //$("#modal-add-chargeback").modal('hide');
        $("#modal-create-chargeback").modal({backdrop: 'static', keyboard: false});

    }

    var initialiseEvents = function initialiseEvents() {

        $("#modal-add-chargeback .close-chargeback-modal").unbind("click").click(function(e){
           /* e.preventDefault();
            e.preventBubble();*/
         //   $(this).closest('.modal').modal("hide");
            $("#modal-add-chargeback").modal('hide');
            resetChargebackCreationModal();
        });

        $("#modal-create-chargeback .close-chargeback-modal-confirmation").unbind("click").click(function(){
            resetConfirmationModal();
            $("#modal-create-chargeback").modal('hide');
        })
   /*     $("#tx-details-modal").on('hide.bs.modal', function (e) {

         /!*   $("#modal-add-chargeback").find("input").val("");
            $('#chargeBackReason').val("");
            $("#modal-add-chargeback").find("select").selectpicker('refresh');*!/

        });

/!*
        $("#modal-add-chargeback").on('show.bs.modal',function () {
            console.log("modal-add-chargeback");
            $("#tx-details-modal").hide();
        })*!/



        /!*$('#chargeBackReason').on('changed.bs.select', function(e) {
            console.log('Selected elements: ' + ($('#cat').val() || ['Nothing selected']).join(', '));
        });*!/*/

        $("#modal-add-chargeback .proceed-chb-confirm").unbind("click").click(function(){

            var chargeBackReason = $.trim($("#chargeBackReason").val());
            var providerTxnId = $.trim($("#providerTxnId").val());
            var chargebackCaseId = $.trim($("#chargebackCaseId").val());
            var validationFlag = true;
            var letters = /^[0-9a-zA-Z]+$/;
            $("#modal-add-chargeback .create-chb-error").text("");
                if(chargeBackReason == "")
                {
                    $("#chargeBackReason").closest("td").find(".create-chb-error").text("Please select any Reason");
                    $("#chargeBackReason").closest("td").find(".create-chb-error").show();
                    validationFlag =  false;
                }

                if(providerTxnId == "")
                {
                    $("#providerTxnId").parent().find(".create-chb-error").text("Please fill out this field");
                    $("#providerTxnId").parent().find(".create-chb-error").show();
                    validationFlag =  false;
                }
                else if(providerTxnId.match(letters) == null)
                {
                    $("#providerTxnId").parent().find(".create-chb-error").text("Only Alphanumeric characters are allowed");
                    $("#providerTxnId").parent().find(".create-chb-error").show();
                    validationFlag =  false;
                }
                if(chargebackCaseId == "")
                {
                    $("#chargebackCaseId").parent().find(".create-chb-error").text("Please fill out this field");
                    $("#chargebackCaseId").parent().find(".create-chb-error").show();
                    validationFlag =  false;
                }
                else if(chargebackCaseId.match(letters) == null)
                {
                    $("#chargebackCaseId").parent().find(".create-chb-error").text("Only Alphanumeric characters are allowed");
                    $("#chargebackCaseId").parent().find(".create-chb-error").show();
                    validationFlag =  false;
                }

                if(validationFlag)
                {
                    $(".create-chb-error").hide();
                    drawConfirmationModal();
                }

                   return false;

        });



      $("#confirm-chargeback-btn").unbind("click").click(function(){

          var reqObject = {};
          reqObject.chargebackTransactionReference = txData.transactionInfo.dalberryTxnReference;
          reqObject.transactionResponseMessageId =  $("#chargeBackReason").val();
          reqObject.trackCode = $("#providerTxnId").val();
          reqObject.caseId = $("#chargebackCaseId").val();

          txAction.addChargeback(reqObject).then(function (data) {
              if(data.hasOwnProperty("status") && data.hasOwnProperty("message"))
              {
                  if(data.status == 4)
                  {
                      txData.transactionInfo.isChargeBack = true;
                      $("#chargeBackButton").attr("disabled", "disabled");
                       notificationService.notifySuccess('Chargeback Request Successful!');
                      $("#modal-create-chargeback").modal('hide');
                      $("#modal-add-chargeback").modal('hide');
                      $('#loader-container').fadeOut('fast');
                         resetChargebackCreationModal();
                         transactionTable.getTransactionInfo();
                  }
                  else
                  {
                      notificationService.notifyDanger(data.message);

                  }
              }
              else
              {

                  notificationService.notifyDanger('No response from server!Please try again!');

              }


          }, function (data) {

              $('.chargeback-error').text('Request Failed');
              $('.chargeback-success').text('');
          });
      });

    };

    return {
        init: init,
        createChargeBack: createChargeBack

    };
}();
