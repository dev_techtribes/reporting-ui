var transactionTable = (function () {
    var resetHeaderInfo = function () {
        env.optionalHeaderListTxInfo = ["type", "paymentInstrument", "txnCreatedDate", "cardHolderName", "cardBrand", "paymentMethod", "ip", "3dSecure", "billingDescription", "revertedTxn", "platform", "browser", "providerTxnID", "authCode", "errorCode", "errorMessage", "dbErrorMessage", "dbErrorCode"];
        env.optionalHeaderListTxInfoCloned = ["type", "paymentInstrument", "txnCreatedDate", "cardHolderName", "cardBrand", "paymentMethod", "ip", "3dSecure", "billingDescription", "revertedTxn", "platform", "browser", "providerTxnID", "authCode", "errorCode", "errorMessage", "dbErrorMessage", "dbErrorCode"];
        //customer optional columns
        env.optionalHeaderListCustInfo = ["firstName", "lastName", "customerEmail", "issuingBank", "issuingCountry", "phone", "birthDate", "address", "city", "state", "country", "isMale", "mac", "postalCode"];
        env.optionalHeaderListCustInfoCloned = ["firstName", "lastName", "customerEmail", "issuingBank", "issuingCountry", "phone", "birthDate", "address", "city", "state", "country", "isMale", "mac", "postalCode"];
    };
    var freezeTableHeader = function () {
        $(window).scroll(function () {
            var translate = "translate(0," + $(this).scrollTop() + "px)";
            $(".transactions-table").find("th").css("transform", translate);
            $(".transactions-table").find("th").css("-webkit-transform", translate);
            $(".transactions-table").find("th").css("-moz-transform", translate);
            $(".transactions-table").find("th").css("-ms-transform", translate);
            filters.setDropDownHeight();
        });
    }
    var markLastColumn = function () {
        $(".dashboard .transactions-table th").not('.hide').removeClass('last');
        $(".dashboard .transactions-table th").not('.hide').last().addClass('last');
        $(".dashboard .transactions-table th").not('.hide').eq(-2).addClass('last');
    };
    var updateTableData = function () {
        var data = env.tempCollection;

        console.log(data);

        if ($('.transactions-table:not(.scroll-table)').length && data.transactions) {


            var markup = "";

            if (data.transactions != null) {
                for (var i = 0; i < data.transactions.length; i++) {
                    data.transactions[i].index = i;
                    d = data.transactions[i].transactionInfo;
                    c = data.transactions[i].customerInfo;
                    /*var birthDate = "";
                    if(c.birthDate != "****")
                    {
                        var dob =  new Date(c.birthDate.replace('.','/').replace('.','/'));
                        birthDate = moment(dob).format("DD:MMM:YYYY");
                    }
                    else
                    {
                        birthDate = c.birthDate;
                    }*/

                    var st = data.transactions[i].transactionInfo.txnStatus;
                    var m;
                    if (st == 'Approved' || st == 'Authorized' || st == 'Cancelled' || st.indexOf("Completed") != -1) {
                        m = '<span class="status approved">' + data.transactions[i].transactionInfo.txnStatus + '</span>';
                    }
                    else if (st.indexOf('In progress') != -1 || st.indexOf("Initiated") != -1 || st.indexOf("Pending") != -1) {
                        m = '<span class="status pending">' + data.transactions[i].transactionInfo.txnStatus + '</span>';
                    }
                    else if (st == 'Failed' || st == 'Declined' || st == 'GW Decline' || st == 'Abandoned' || st == 'Authorize Expired' || st == 'Expired' || st.indexOf("Rejected") != -1 || st.indexOf("Decline") != -1) {
                        m = '<span class="status declined">' + data.transactions[i].transactionInfo.txnStatus + '</span>';
                    }
                    else {
                        m = '<span class="status">' + data.transactions[i].transactionInfo.txnStatus + '</span>';
                    }
                    if (d.txnType != null && d.txnType == "Refund"
                        || d.txnType == "Chargeback"
                        || d.txnType == "Withdrawal") {
                        if (d.txnAmount != null && d.txnAmount > 0) {
                            d.txnAmount = parseFloat(d.txnAmount * -1.0);
                            d.txnAmount = d.txnAmount.toFixed(2);
                            d.txnAmount = d.txnAmount.toLocaleString("en-US");
                        }
                    } else {
                        console.log("not reached :: ", d.txnType);
                    }

                    var formattedAmount = formatAmounts(d.txnAmount);
                    var isMaleFlag = data.transactions[i].customerInfo.isMale;
                    var gender = "";
                    if (isMaleFlag == "N/A" || isMaleFlag == null) {
                        gender = "N/A";
                    }
                    else {
                        if (isMaleFlag) {
                            gender = "M";
                        }
                        else {
                            gender = "F";
                        }
                    }
                    var is3d = data.transactions[i].transactionInfo.is3d;
                    var flag3d = "";
                    console.log("is3d ::"+is3d);
                    if(is3d == null || is3d == '' || is3d == 'N/A'){
                        flag3d = "N/A";
                    }
                    if(is3d == true){
                        flag3d = "Yes";
                    }
                    else{
                        flag3d = "No";
                    }
                    /*var flag3d = "";
                    if (is3d != "N/A"){
                        if (is3d == 1) {

                            flag3d = "Yes";
                        }
                        else if (is3d == 0) {
                            flag3d = "No";
                        }
                    }
                    else {
                        flag3d = "N/A";
                    }*/
                    var iconMarkup = "";


                    iconMarkup = "<a class='ico-doc'>&#32;</a>";
                    console.log("data.transactions[i].transactionInfo.isFraud  :" + data.transactions[i].transactionInfo.isFraud);
                    if (data.transactions[i].transactionInfo.isNotesAvailable == true) {
                        iconMarkup = "<a class='ico-doc'><span class='ico-notif'>&#32;</span></a>";
                    }
                    if (data.transactions[i].transactionInfo.isFraud == true) {
                        iconMarkup = "<a class='ico-notif-red'>&#32;</a>";
                    }

                    var temp = $("#row-template").text();
                    var row = temp.replace('$index$', i)
                        .replace('$index$', i)
                        .replace('$index$', i)
                        .replace('$index$', i)
                        .replace('$iconMarkup$', iconMarkup)
                        .replace('$txnProcessedDate$', d.txnProcessedDate)
                        .replace('$merchantCustomerId$', d.merchantCustomerId)
                        .replace('$txnAmount$', formattedAmount)
                        .replace('$txnCurrency$', d.txnCurrency)
                        .replace('$cardType$', d.cardType.toLowerCase())
                        .replace('$txnType$', d.txnType)
                        .replace('$m$', m)
                        .replace('$dalberryTxnReference$', d.dalberryTxnReference)
                        .replace('$merchantTxnId$', d.merchantTxnId)
                        .replace('$merchant$', d.merchant)
                        .replace('$brand$', d.brand)
                        .replace('$mid$', d.mid)
                        .replace('$txnCreatedDate$', d.txnCreatedDate)
                        .replace('$cardHolderName$', d.cardHolderName)
                        .replace('$paymentInstrument$', d.paymentInstrument)
                        .replace('$paymentMethod$', d.paymentMethod)
                        .replace('$ip$', d.ip)
                        .replace('$flag3d$', flag3d)
                        .replace('$billingDescriptor$', d.billingDescriptor)
                        .replace('$revertedTxn$', d.revertedTxn)
                        .replace('$platform$', d.platform)
                        .replace('$browser$', d.browser)
                        .replace('$providerTxnID$', d.providerTxnId)
                        .replace('$authCode$', d.authCode)
                        .replace('$errorCode$', d.errorCode)
                        .replace('$errorMessage$', d.errorMessage)
                        .replace('$dbErrorCode$', d.dbErrorCode)
                        .replace('$dbErrorMessage$', d.dbErrorMessage)
                        .replace('$firstName$', c.firstName)
                        .replace('$lastName$', c.lastName)
                        .replace('$customerEmail$', c.customerEmail)
                        .replace('$phone$', c.phone)
                        .replace('$birthDate$', c.birthDate)
                        .replace('$gender$', gender)
                        .replace('$address$', c.address)
                        .replace('$city$', c.city)
                        .replace('$state$', c.state)
                        .replace('$postalCode$', c.postalCode)
                        .replace('$country$', c.country)
                        .replace('$macAddress$', c.macAddress)
                        .replace('$issuingBank$', 'N/A')
                        .replace('$issuingCountry$', 'N/A');
                    markup = markup + row;
                }

            }

            if (data.transactions == null || data.transactions.length == 0) {
                markup = '<tr><td colspan="2" class="norecords disable-selection">No records found</td></tr>';
                $(".dataTables_paginate").pagination('destroy');
                miniSummary.resetMiniSummary();
            }

            var tableOffset = $(".transactions-table").offset().top;
            var $header = $(".transactions-table > thead").clone();
            var $fixedHeader = $("#header-fixed").append($header);
            $(".transactions-table tbody").html(markup);
           /* if (data.transactions != null || data.transactions.length != 0) {
                $('.transactions-table tbody tr').on('click', function (e) {
                    $this = $(this);
                    $this.toggleClass('active');
                });
            }*/
            $(window).bind("scroll", function () {
                var offset = $(this).scrollTop();
                if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                    $fixedHeader.show();
                }
                else if (offset < tableOffset) {
                    $fixedHeader.hide();
                }
            });
            //$('.transactions-table').tablesorter();


            $.each(env.optionalHeaderListTxInfo, function () {
                var currentId = '.' + this;
                $(currentId).hide();
                $(currentId).addClass('hide');
            });

            $.each(env.optionalHeaderListCustInfo, function () {
                var currentId = '.' + this;
                $(currentId).hide();
                $(currentId).addClass('hide');
            });
            if (data.transactions.length > 0) {
                //  $('.transactions-table-container').style("overflow","auto");
                paginate.init(data.count, 25);
                if (data.quickSummary != null) {
                    if(env.updatedByQuickSummary == 1){
                     env.updatedByQuickSummary = 0;
                     miniSummary.init(data.quickSummary);
                    }
                    // console.log("change-----------",data.transactions);
                    // updateMiniSummary(data.transactions);
                }

            }


        }
        markLastColumn();
        // rbac.updateRoles();
        return false;

    }

    var updateMiniSummary = function (transactions) {
        console.log(transactions);

        var summaryList = [];
        var currencyList = [];

        var data = transactions.slice();
        for (var k = 0; k < data.length; ++k) {
            var SummaryData = {
                currency: null,
                incomingAmount: 0,
                incomingTransactionCount: 0,
                outgoingAmount: 0,
                outgoingTransactionCount: 0,
                revenue: 0,
                totalTransactions: 0
            }

            var currency = data[k].transactionInfo.txnCurrency;
            // console.log("currency :: ",currency);
            // console.log("currencyList length :: ",currencyList.length);
            if (currencyList.indexOf(currency) == -1) {
                if (data[k].transactionInfo.txnType == "Sale" ||
                    data[k].transactionInfo.txnType == "Capture") {
                    console.log("Sale Init");
                    SummaryData.incomingAmount = parseFloat(Math.abs(data[k].transactionInfo.txnAmount));
                    SummaryData.incomingTransactionCount = 1;
                    SummaryData.revenue = parseFloat(Math.abs(data[k].transactionInfo.txnAmount));
                } else {
                    console.log("Non - Sale Init");
                    SummaryData.outgoingAmount = parseFloat(Math.abs(data[k].transactionInfo.txnAmount));
                    SummaryData.outgoingTransactionCount = 1;
                    SummaryData.revenue = parseFloat(data[k].transactionInfo.txnAmount);
                }

                SummaryData.currency = currency;
                SummaryData.totalTransactions = 1;
                summaryList.push(SummaryData);
                currencyList.push(currency);
            } else {
                for (var i = 0; i < summaryList.length; ++i) {
                    if (summaryList[i].currency == currency) {
                        console.log("Update");
                        if (data[k].transactionInfo.txnType == "Sale" ||
                            data[k].transactionInfo.txnType == "Capture") {
                            summaryList[i].incomingAmount = parseFloat(Math.abs(summaryList[i].incomingAmount)) + parseFloat(Math.abs(data[k].transactionInfo.txnAmount));
                            summaryList[i].incomingTransactionCount = parseFloat(Math.abs(summaryList[i].incomingTransactionCount)) + 1;
                            // summaryList[i].revenue = parseFloat(Math.abs(summaryList[i].revenue)) + parseFloat(Math.abs(data[k].transactionInfo.txnAmount));
                            summaryList[i].totalTransactions = parseFloat(Math.abs(summaryList[i].totalTransactions)) + 1;
                        } else {
                            console.log("Non - Sale Update");
                            summaryList[i].outgoingAmount = parseFloat(Math.abs(summaryList[i].outgoingAmount)) + parseFloat(Math.abs(data[k].transactionInfo.txnAmount));
                            summaryList[i].outgoingTransactionCount = parseFloat(Math.abs(summaryList[i].outgoingTransactionCount)) + 1;
                            // summaryList[i].revenue = parseFloat(Math.abs(summaryList[i].revenue)) - parseFloat(Math.abs(data[k].transactionInfo.txnAmount));
                            summaryList[i].totalTransactions = parseFloat(Math.abs(summaryList[i].totalTransactions)) + 1;
                        }
                        // console.log(summaryList[i].revenue);
                        summaryList[i].revenue = parseFloat(Math.abs(summaryList[i].incomingAmount)) - parseFloat(Math.abs(summaryList[i].outgoingAmount));
                    }
                }
            }
        }

        miniSummary.init(summaryList);

        // env.quickSummary = summaryList;
        // console.log("quickSummary 1 : ",env.quickSummary);
        // miniSummary.init(env.quickSummary);
        // console.log("quickSummary 3: ",env.quickSummary);
    }

    var bindEvents1 = function () {
        $('.comment-form textarea').each(function () {
            if (this.value.length >= 1) {
                $(this).closest('.comment-form').find('.btn-primary').removeAttr('disabled');
            } else {
                $(this).closest('.comment-form').find('.btn-primary').attr('disabled', 'disabled');
            }
        });
        calcSearchWidth();
        //calcInfoSummaryWidth();
    };
    var bindEvents2 = function () {
        appear({
            elements: function elements() {
                return document.getElementsByClassName('appear');
            },
            appear: function appear(el) {
                var item = $(el);
                $('body').addClass('unfixed-summary');
            },
            disappear: function disappear(el) {
                var item = $(el);
                $('body').removeClass('unfixed-summary');
            },
            bounds: 0,
            reappear: true,
            deltaTimeout: 50
        });
        var TIMEOUT = 300;
        if ($('.transactions-area').find(' > .summary-box').length) {
            $('.transactions-area').wrapInner('<div class="page-area"/>')
        }
        $('<span class="fader"/>').appendTo('#wrapper').css('opacity', 0);
        $('.btn-menu').click(function () {
            if (!$('body').hasClass('open-menu')) {
                $('body').addClass('open-menu');
                $('.fader').css('display', 'block').animate({
                    opacity: 1,
                    right: '300px'
                }, TIMEOUT);
                $('#wrapper').animate({ right: '300px' }, TIMEOUT);
                $('.tools-panel').animate({ right: '300px' }, TIMEOUT);
                $('#main-nav').animate({ 'margin-right': 0 }, TIMEOUT);
            } else {
                $('.fader').animate({
                    opacity: 0,
                    right: 0
                }, TIMEOUT, function () {
                    $(this).css('display', 'none');
                    $('body').removeClass('open-menu');
                });
                $('#wrapper').animate({ right: 0 }, TIMEOUT);
                $('.tools-panel').animate({ right: 0 }, TIMEOUT);
                $('#main-nav').animate({ 'margin-right': '-300px' }, TIMEOUT);
            };
            return false;
        });
        $('.fader').click(function () {
            $('.fader').animate({
                opacity: 0,
                right: 0
            }, TIMEOUT, function () {
                $(this).css('display', 'none');
                $('body').removeClass('open-menu');
            });
            $('#wrapper').animate({ right: 0 }, TIMEOUT);
            $('.tools-panel').animate({ right: 0 }, TIMEOUT);
            $('#main-nav').animate({ 'margin-right': '-300px' }, TIMEOUT);
        });
        $('<div class="header-btns visible-sm-block visible-xs-block">').appendTo('#main-nav').insertAfter('#main-nav > ul');
        $('#header .help-link, #header .notifications-box, #header .logout-link').clone().removeClass('hidden-sm hidden-xs').appendTo('#main-nav .header-btns');

        $('.filters-nav .mobile-opener').click(function () {
            $(this).closest('.filters-nav').toggleClass('opened');
            $(this).closest('.filters-nav').find('> ul').slideToggle(300);
            return false;
        });

        /*    $('.only-date').daterangepicker({
                "linkedCalendars": false,
                "autoUpdateInput": true,
                "showDropdowns": false,
                "singleDatePicker": true,
                "showCustomRangeLabel": false,
                "buttonClasses": "btn",
                "applyClass": "btn-primary",
                "cancelClass": "btn-link",
            });*/

        /*  var scrollTable;
          $('.scroll-table-modal').on('shown.bs.modal', function (e) {

              var tableHeight = parseInt($(this).find('.modal-content').css("max-height")) - $(this).find('.modal-header').outerHeight() - $(this).find('.transactions-table th').outerHeight();
              scrollTable = $('.transactions-table.scroll-table').DataTable({
                  dom: '<"table-wrap"rt>',
                  "scrollY": tableHeight,
                  "scrollX": true,
                  scrollCollapse: true,
                  paging: false
              });

          }).on('hide.bs.modal', function (e) {

              scrollTable.destroy();

          });*/

        $('.comment-form textarea').keyup(function () {
            if (this.value.trim().length >= 1) {
                $(this).closest('.comment-form').find('.btn-primary').removeAttr('disabled');
            } else {
                $(this).closest('.comment-form').find('.btn-primary').attr('disabled', 'disabled');
            }
        });
        $('.modal-content').css('max-height', $(window).outerHeight() - 40);
        $("#tx-details-modal").on('show.bs.modal', function (e) {
            $('.comment-form textarea').textPlaceholder();
        });

        $("#tx-details-modal").on('shown.bs.modal', function (e) {
            e.stopPropagation();
            $('.comment-form textarea').textPlaceholder();
            $('#tx-details-modal .modal-content').niceScroll().remove();
            $('#tx-details-modal .modal-content').getNiceScroll().hide();
            $('#tx-details-modal .modal-content').niceScroll({
                scrollspeed: 200,
                mousescrollstep: 40,
                autohidemode: false,
                cursorwidth: 3,
                cursorcolor: "#999",
                touchbehavior: false,
                cursorborder: "0",
                railpadding: {
                    top: 5,
                    right: 3,
                    left: 0,
                    bottom: 5
                },
                cursorborderradius: "2px"
            });
            $('#tx-details-modal .modal-content').getNiceScroll().hide();
            $('#tx-details-modal .modal-content').getNiceScroll().show();
        }).on('hide.bs.modal', function (e) {

            $('#tx-details-modal .modal-content').scrollTop(0);
            $(".chargeback-success").text("");
            $(".chargeback-error").text("");
            $("transaction-log tbody").html("");
            // $('#tx-details-modal .modal-content').niceScroll().remove();

        });

        $('.summary-box.transperent').mouseenter(function () {
            $(this).css("opacity", "1");
        });
        $('.summary-box.transperent').mouseleave(function () {
            $(this).css("opacity", "0.5");
        });
        // $('.summary-box.transperent .opener').mouseenter(function(){
        //     $('.summary-box.transperent').css("opacity","1");
        // });
        // $('.summary-box.transperent .opener').mouseleave(function(){
        //     $('.summary-box.transperent').css("opacity","0.5");
        // });
        $('.summary-box .opener').click(function (e) {

            if ($('.summary-box .opener').closest('.summary-box').hasClass("open")) {
                $(this).parent().find('.button-arrow').siblings('table').find('tr:nth-child(n+2) > td > .cell').slideUp();
                //$(this).parent().find('.button-arrow').removeClass('active');
                $('.summary-box.transperent').css("opacity", "0.5");
                $('.summary-box .opener').blur();
                $('.summary-box .opener').closest('.summary-box').blur();
            }
            else {
                $('.summary-box.transperent').css("opacity", "1")
            }
            $('.summary-box .opener').closest('.summary-box').toggleClass('open');

            e.stopPropagation();
        });
        /*$('.summary-box table').find('td').wrapInner('<div class="cell"></div>');
        $('.summary-box .button-arrow').click(function () {
            $(this).siblings('table').find('tr:nth-child(n+3) > td > .cell').slideToggle();
            $(this).toggleClass('active');
        });
        var length = $('.info-summary table th').length,
            cont;

        for (var i = 0; i < length; i++) {
            cont = $('.info-summary table th').eq(i).wrapInner('<div class="th-xs"></div>').html();

            $('.info-summary table tr').each(function () {
                $(this).find('.cell').eq(i).prepend(cont);
            });
        }*/
        $('.calendar-box .date-select .btn-list').click(function () {
            if ($(this).closest('th').find('.date-select').length) {
                $(this).closest('th').find('.date-select .date-range .form-control').click();
            }
            $('.daterangepicker').on('click', function (event) {
                console.log("new daterange");
                event.stopPropagation();
            });
            return false;
        });
        /*$('.daterangepick').daterangepicker({
            "linkedCalendars": false,
            "autoUpdateInput": true,
            "showDropdowns": true,
            "showCustomRangeLabel": false,
            "buttonClasses": "btn",
            "applyClass": "btn-primary",
            "cancelClass": "btn-link",
            "startDate": "10/27/2016",
            "endDate": "11/28/2016"
        }, function (start, end, label) {
            console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        });
*/
        if ($(window).width() > 1024) {

            $('#sidebar .scrollbar').niceScroll({
                scrollspeed: 200,
                mousescrollstep: 40,
                autohidemode: false,
                cursorwidth: 3,
                cursorcolor: "#999",
                touchbehavior: true,
                grabcursorenabled: false,
                cursorborder: "0",
                railpadding: {
                    top: 5,
                    right: 3,
                    left: 0,
                    bottom: 5
                },
                cursorborderradius: "2px"
            });
        }
        $('[type=checkbox]').addClass('invisible').after('<div class="custom-check"></div>');
        $('.check-all').click(function () {
            $(this).closest('footer').siblings('.check-list').find("input:checkbox").not(":disabled").prop('checked', true);
        });

        $('#sidebar .slides').slick({
            slide: '.slide',
            infinite: false
        });
        $('<span class="overlay"/>').appendTo('#wrapper .tools-panel');
        $('.menu-opener').click(function () {
            $(this).removeClass("change");
            $(this).removeClass("black");
            var ul = $("#change-password").closest("ul");
            ul.fadeOut();
            if (!$('html').hasClass('push')) {
                $('html').addClass('push');
                setTimeout(function () {
                    $('html').addClass('opening');
                }, 0);
                $(this).addClass('opened');
                $('.overlay').fadeIn(TIMEOUT);
                //$(this).css({ "background": "#fff", "border": "solid 1px #fff" })
                $('#sidebar').css("transform", "perspective(300px) rotateY(90deg)")
                $('#sidebar').addClass('sidebar-opened');
            } else {
                // $(this).removeClass('opened');
                // $('.overlay').fadeOut(TIMEOUT);
                // $('html').removeClass('opening');
                // setTimeout(function () {
                //     $('html').removeClass('push');
                // }, TIMEOUT);
                sideBar.hideSideBar();
                //$(this).css({ "background": "#9d9d9d", "border": "solid 1px #9d9d9d" });

                //  $('#sidebar').css({"transform": "perspective(300px) rotateY(0deg)"})
                $('#sidebar').removeClass('sidebar-opened');
                $('#sidebar').css({ "transform": "perspective(300px) rotateY(0deg)" })
                //$('#sidebar').css({"transform": ""})

            };
            return false;
        });
        $('.overlay').click(function () {
            // $('.menu-opener').removeClass('opened');
            // $('.overlay').fadeOut(TIMEOUT);
            // $('html').removeClass('opening');
            // setTimeout(function () {
            //     $('html').removeClass('push');
            // }, TIMEOUT);
            sideBar.hideSideBar();
        });

        $('.payment-box .clear-filters').click(function () {
            $(this).closest('.payment-box').find('.check-group li:first-child .checkbox input:checkbox').prop('checked', true);
            $(this).closest('.payment-box').find('.check-group li + li .checkbox input:checkbox').prop('checked', false);
        });
        $('.payment-box .check-group li + li .checkbox input:checkbox').change(function () {
            if ($(this).is(':checked')) {
                $(this).closest('.check-group').find('li:first-child .checkbox input:checkbox').prop('checked', false);
            }
        });

        $('.filters-nav > ul > li').has('ul').each(function () {
            $('<span class="opener"/>').appendTo($(this).find('> a'));
        });
        $('.filters-nav > ul > li').has('ul').addClass('has-drop');
        $('.filters-nav .has-drop > a').click(function () {
            if ($(window).width() > 991) {
                if ($(this).parent().hasClass('current')) {
                    $(this).parent().removeClass('current');
                } else {
                    $(this).parent().addClass('current');
                    $(this).parent().siblings('.current').removeClass('current');
                };
            } else {
                if ($(this).parent().hasClass('current')) {
                    $(this).parent().removeClass('current').children('ul').slideUp(TIMEOUT);
                } else {
                    $(this).parent().addClass('current').children('ul').slideDown(TIMEOUT);
                    $(this).parent().siblings('.current').removeClass('current').children('ul').slideUp(TIMEOUT);
                };
            }
            return false;
        });

        $('.tools-panel .mobile-opener').html($('.filters-nav > ul').find(' > .active > a').text());

        $('.dispute-form form').validate({
            errorClass: "error",
            validClass: "valid",
            highlight: function (element) {
                $(element).addClass('error').removeClass('valid');
            },
            unhighlight: function (element) {
                $(element).removeClass('error').addClass('valid');
            },
            onclick: function (element) {
                if ($('.dispute-form form').valid()) {
                    $('.dispute-form .btn').removeClass('disabled');
                } else {
                    $('.dispute-form .btn').addClass('disabled');
                }
            },
            onkeyup: function (element, event) {
                if ($('.dispute-form form').valid()) {
                    $('.dispute-form .btn').removeClass('disabled');
                } else {
                    $('.dispute-form .btn').addClass('disabled');
                }
            },
            showErrors: function (errorMap, errorList) {
                if (this.numberOfInvalids()) {
                    $('.dispute-form .btn').addClass('disabled');
                } else {
                    $('.dispute-form .btn').removeClass('disabled');
                }
                this.defaultShowErrors();
            },
            rules: {
                'radio[]': {
                    required: true,
                    maxlength: 2
                },
                comment: {
                    required: true
                },
            },
        });

        $('.blacklist-form form').validate({
            errorClass: "error",
            validClass: "valid",
            highlight: function (element) {
                $(element).addClass('error').removeClass('valid');
            },
            unhighlight: function (element) {
                $(element).removeClass('error').addClass('valid');
            },
            onkeyup: function (element, event) {
                if ($('.blacklist-form form').valid()) {
                    $('.blacklist-form .btn').removeClass('disabled');
                } else {
                    $('.dispute-form .btn').addClass('disabled');
                }
            },
            showErrors: function (errorMap, errorList) {
                if (this.numberOfInvalids()) {
                    $('.blacklist-form .btn').addClass('disabled');
                } else {
                    $('.blacklist-form .btn').removeClass('disabled');
                }
                this.defaultShowErrors();
            },
            rules: {
                comment: {
                    required: true
                },
            },
        });

        $('.checkbox-ios').append('<label class="control-indicator"></label>');

        $('.radio [type=radio]').addClass('invisible').after('<div class="custom-radio"></div>');

        $('.contact-form form').validate({
            errorClass: "error",
            validClass: "valid",
            highlight: function (element) {
                $(element).addClass('error').removeClass('valid');
            },
            unhighlight: function (element) {
                $(element).removeClass('error').addClass('valid');
            },
            onkeyup: function (element, event) {
                if ($('.contact-form form').valid()) {
                    $('.contact-form .btn').removeClass('disabled');
                } else {
                    $('.contact-form .btn').addClass('disabled');
                }
            },
            showErrors: function (errorMap, errorList) {
                if (this.numberOfInvalids()) {
                    $('.contact-form .btn').addClass('disabled');
                } else {
                    $('.contact-form .btn').removeClass('disabled');
                }
                this.defaultShowErrors();
            },
            submitHandler: function (form) {
                $('.contact-form').addClass('visible-block');
                // $(form).submit();
            },
            rules: {
                'radiogr[]': {
                    required: true,
                    maxlength: 2
                },
                subject: {
                    required: true,
                    maxlength: 50
                },
                description: {
                    required: true,
                    maxlength: 100
                },
            },
        });

        $('.info-section .img').each(function () {
            if ($(this).find('> img').length) {
                $(this).css('background-image', 'url(' + $(this).find('> img').attr('src') + ')').find('> img').hide();
            }
        });

        $('.FixedHeader_Cloned label').each(function (index, el) {
            var forLabel = $(el).attr('for') + 'clone';
            $(el).attr('for', forLabel);
            $(el).parent().find('.invisible').attr('id', forLabel);
        });
        destroyNiceScroll();


        $('.sorting').click(function () {
            $(this).find('.table-sort').animateCss('flash');
        });

        $('.table-dropdown .btn-primary').click(function (event) {

            var customerId = $('#customerid-val').val();
            var amountFrom = $('#amount-from').val();
            var amountTo = $('#amount-to').val();
            var dlbrefId = $('#dlbtxnid-val').val();
            var merchanttxnId = $('#merchanttxnid-val').val();
            var currencyList = [];
            if (this.id == 'amt-filter-btn') {
                if (!(amountFrom == '' && amountTo == '') && (amountFrom == '' || amountTo == '')) {
                    $('.amt-error').text('Please provide value for both fields');
                    event.stopImmediatePropagation();
                    return;
                }
                if ((parseFloat(amountFrom) >= 0 && parseFloat(amountTo) >= 0) || isNaN(amountFrom) || isNaN(amountTo)) {
                    if (parseFloat(amountFrom) > parseFloat(amountTo)) {
                        $('.amt-error').text('Invalid Amount Values');
                        event.stopImmediatePropagation();
                        return;
                    }
                } else {
                    if (Math.abs(parseFloat(amountFrom)) == 0 && parseFloat(amountTo) < 0) {
                        $('.amt-error').text('Invalid Amount Values');
                        event.stopImmediatePropagation();
                        return;
                    }
                    if (parseFloat(amountFrom) > 0 && parseFloat(amountTo) < 0) {
                        $('.amt-error').text('Invalid Amount Values');
                        event.stopImmediatePropagation();
                        return;
                    }
                    else if (parseFloat(amountFrom) < 0 && parseFloat(amountTo) < 0 && parseFloat(amountFrom) > parseFloat(amountTo)) {
                        $('.amt-error').text('Invalid Amount Values');
                        event.stopImmediatePropagation();
                        return;
                    }
                }

            }
            $(this).parents('.table-dropdown').fadeOut();
        });

        $(document).on('click', function (e) {
            var filterButton = $('.drop-opener');
            var container = $('.table-dropdown, .daterangepicker');
            if (!filterButton.is(e.target) && container.has(e.target).length === 0) {
                $('.transactions-table .drop-opener').each(function () {
                    var current = $(this).data("filter-type");
                    if (env.activeFilterList[current] == false || env.activeFilterList.hasOwnProperty(current) === false) {
                        $(this).removeClass('active');
                    }
                });

                $('.table-dropdown').find(".check-group").scrollTop("0").scrollLeft("0");
                $('.table-dropdown').find(".option-filter-search input[type=search].form-control").val("");
                $('.table-dropdown').fadeOut();
                //$('.transactions-table-container').css("overflow","auto");
            }

            if (!$(e.target).is(".user .dropdown")) {
                if ($('.user .dropdown').is(':visible')) {
                    var ul = $("#change-password").parent().parent();
                    ul.fadeOut();
                }
            }

           /* var miniSummary = $('.summary-box');
            if(!miniSummary.is(e.target.closest(".summary-box")))
            {
                $(".summary-box").removeClass("open");
            }*/

            closeUserMenu(e);
            // $(".menu-opener").css({ "background": "#9d9d9d", "border": "solid 1px #9d9d9d" });
            /*console.log(e);
            var summarybox = $('.summary-box');
            var openerButton = $('.summary-box .opener');
            if(!summarybox.is(e.target))
            {
             if(summarybox.hasClass('open')){
                $('.summary-box').removeClass('open');
             }
            }*/
        });

        $('.dropdown').on('click', function (e) {
            $this = $(this);
            if (!$this.hasClass('active')) {
                $this.addClass('active');
                $this.find('.dropdown-menu').show();
                $this.find('.dropdown-menu').animateCss('bounceInDown');
            } else {
                $this.removeClass('active');
                $this.find('.dropdown-menu').fadeOut();
            }
            return false;
        });

        $('.transactions-table tbody tr').on('click', function (e) {
            if($(this).find(".disable-selection").length == 0)
            {
                $this = $(this);
                $this.toggleClass('active');
            }

        });

    };

    function loadTransactionPage() {
        var requestObj = env.getTransactionsRequestObject;
        var index = 1;
        var startDate = moment().subtract(24, 'hours').format("YYYY-MM-DD HH:mm:ss");
        var endDate = moment().format("YYYY-MM-DD HH:mm:ss");
        $('#backOfficeAgentName').text(env.agentName);
        requestObj.startDate = startDate;
        requestObj.endDate = endDate;
        $.each(env.optionalHeaderListTxInfo, function () {
            var currentId = '.' + this;
            $(currentId).hide();
        });

        $.each(env.optionalHeaderListCustInfo, function () {
            var currentId = '.' + this;
            $(currentId).hide();
        });
        //transactionTable.init();
        //searchBox.init();
        changePassword.init();
        env.updatedByPagination = false;
        transactionTable.getTransactionInfo();



        sideBar.init();
        filters.init();
        dateRange.init();
        exportCsv.init();
        validations.init();



        $('.filters-nav li').click(function (e) {

            $(this).addClass("active");
            filters.resetAllFilters();

            $(this).addClass("active");
            filters.resetAllFilters();

        });





        var table = $('.transactions-table');

        function parseDate(input) {
            var parts = input.split(' ');
            if (parts[0] && parts[2]) {
                var date = parts[0].split('.');
                var time = parts[2].split(':');
                var monthArray = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

                return new Date(date[2], monthArray.indexOf(date[0].toUpperCase()), date[0], time[0], time[1], time[2]);
            }
        }
        $('.transactions-table th .table-sort').not(".transactions-table th:first-child")
            .each(function () {
                var currentElement = $(this);
                var th = $(this).parent(),
                    thIndex = th.index(),
                    inverse = false;
                var dataheader = th.data("header-name");
                var columnKeys = Object.keys(env.sortableColumns);
                if ($.inArray(dataheader, columnKeys) < 0) {
                    return;
                }
                var methoda, methodb;
                currentElement.click(function () {
                    $('.transactions-table th').removeClass('blue');
                    th.toggleClass('blue');
                    th.find('.table-sort').animateCss('flash');
                    env.updatedByPagination = false;
                    env.getTransactionsRequestObject.index = 1;
                    env.getTransactionsRequestObject.sortByColumnName = dataheader;
                    if (env.sortableColumns[dataheader] == "ASC") {
                        env.sortableColumns[dataheader] = "DESC";
                    } else {
                        env.sortableColumns[dataheader] = "ASC";
                    }
                    env.getTransactionsRequestObject.sortType = env.sortableColumns[dataheader];
                    transactionTable.getTransactionInfo();

                });

            });

    };


    var load = function () {
        bindEvents2();
        bindEvents1();
        loadTransactionPage();
    };
    var init = function () {
        jQuery.extend(env.tempTransactionsRequestObject, env.getTransactionsRequestObject);
        $(".transactions-table th").addClass("no-flickr");
        var filterList = $('span .drop-opener').map(function () { return $(this).data("filter-type"); }).get();
       /* $.each(filterList, function () {
            env.activeFilterList[this] = false;
        });*/
        /*$.each($('.transactions-table thead tr th:gt(0)'),function(i,val){
            $(this).hide();
        });*/
        load();

        $('.table-dropdown input').not('.daterangepick').off();
        initialise();
        freezeTableHeader();
        initialiseEvents();
        transactionDetails.init();
        $('.dlb-table-cover').css('min-height', ($(window).height() - 245) + 'px');
        $('.comment-form textarea').textPlaceholder();
        $('a').removeAttr('href');
        /*if(user.isMerchant){
           $('#sidebar li.tran-merchant').attr('style', 'display:block');
           $('#sidebar li.tran-brand').attr('style', 'display:block');
           $('#sidebar li.tran-mid').attr('style', 'display:block');
            env.optionalHeaderListTxInfo.push('merchant');
            env.optionalHeaderListTxInfo.push('brand');
            env.optionalHeaderListTxInfo.push('mid');
            $('#sidebar li.tran-merchant .checkbox input').attr('checked',false);
            $('#sidebar li.tran-brand .checkbox input').attr('checked',false);
            $('#sidebar li.tran-mid .checkbox input').attr('checked',false);
        }
        else{
            $('#sidebar li.tran-merchant').attr('style', 'display:none');
            $('#sidebar li.tran-brand').attr('style', 'display:none');
            $('#sidebar li.tran-mid').attr('style', 'display:none');
            $('#sidebar li.tran-merchant .checkbox input').attr('checked','checked');
            $('#sidebar li.tran-brand .checkbox input').attr('checked','checked');
            $('#sidebar li.tran-mid .checkbox input').attr('checked','checked');
        }*/
        //if (user.department == 'merchant') {
        //    optionalHeaderListTxInfo.push('merchant');
        //   optionalHeaderListTxInfo.push('brand');
        //    optionalHeaderListTxInfo.push('mid');
        //}
        // else {
        //   $('#sidebar li.tran-merchant .checkbox input').attr('checked', 'checked');
        //  $('#sidebar li.tran-brand .checkbox input').attr('checked', 'checked');
        //  $('#sidebar li.tran-mid .checkbox input').attr('checked', 'checked');
        //  }


    };
    filterDataTable = function (data) {
        tx = env.trxTableCollection.transactions;
        var tx = $.extend({}, env.trxTableCollection);
        var list = tx.transactions.filter(function (t) {
            if (data.hasOwnProperty(env.searchBoxLabelCustomerId)) {
                if (t.transactionInfo.merchantCustomerId == data[env.searchBoxLabelCustomerId])
                    return t;
            }
            else if (data.hasOwnProperty(env.searchBoxLabelMethod)) {
                if (t.transactionInfo.cardType == data[env.searchBoxLabelMethod])
                    return t;
            }
            else if (data.hasOwnProperty(env.searchBoxLabelDLBRef)) {
                if (t.transactionInfo.dalberryTxnReference == data[env.searchBoxLabelDLBRef])
                    return t;
            }
            else if (data.hasOwnProperty(env.searchBoxLabelMerchantTxnId)) {
                if (t.transactionInfo.merchantTxnId == data[env.searchBoxLabelMerchantTxnId])
                    return t;
            }
            else if (data.hasOwnProperty(env.searchBoxLabelCcy)) {
                if (t.transactionInfo.txnCurrency == data[env.searchBoxLabelCcy])
                    return t;
            }
            else if (data.hasOwnProperty(env.searchBoxLabelTxnStatus)) {
                if (t.transactionInfo.txnStatus == data[env.searchBoxLabelTxnStatus])
                    return t;
            }
            else if (data.hasOwnProperty(env.searchBoxLabelTxnType)) {
                if (t.transactionInfo.txnType == data[env.searchBoxLabelTxnType])
                    return t;
            }
        });
        tx.transactions = list;
        env.tempCollection = tx;
        console.log("env :: ", env.tempCollection);
        // updateMiniSummary();
        updateTableData();
    }
    resetTable = function (data) {
        /* env.tempCollection = env.trxTableCollection;
         updateTableData();*/
        console.log(data);
        //updateMiniSummary(env.trxTableCollection.transactions);
        $(".transactions-table tbody").html(data);
    }
    initialiseEvents = function () {
        $(".transactions-table ").on('click', '#icon a', function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            var id = $(e.target).closest('td').data("id");
            // var index = parseInt(id.split("_")[1]);

            var detail = env.tempCollection.transactions.filter(function (tr, index) {

                return index == id;
            });
            console.log(detail);
            transactionDetails.loadTransactionModal(detail[0]);

        });
        $(".logout-link").click(function (event) {
            logout();
        });
        // $(".tfansactions-table-container").scroll(function () {
        //     $("thead").offset({ left: -1 * this.scrollLeft+25 });
        // });
    };
    getTransactionInfo = function () {
        env.getTransactionsRequestObject.userId = user.userid;
        transactionService.getTransactionInfo(env.getTransactionsRequestObject).then(function (data) {
            env.trxTableCollection = data;

            // DLB-1341 | TESTING IN PROGRESS | PLS DON'T DELETE
            // console.log(env.trxTableCollection);
            // console.log(env.trxTableCollection.transactions[0].transactionInfo.txnCurrency);
            // env.trxTableCollection.transactions[0].transactionInfo.txnCurrency = "EUR";

            env.tempCollection = env.trxTableCollection;
            var accessRoles = env.tempCollection.accessPermissionHeirarchy;
            if (env.tempCollection.filterElementMap) {
                var keys = Object.keys(env.tempCollection.filterElementMap);
                var filter = Object.keys(env.rbacHeaderInfoMap);
                for (var i = 0; i < filter.length; i++) {

                    if($.inArray( filter[i], keys)<0){

                       if($.inArray(env.rbacHeaderInfoMap[filter[i]],env.optionalHeaderListTxInfo)<0){
                            env.optionalHeaderListTxInfo.push(env.rbacHeaderInfoMap[filter[i]]);
                            if($.inArray(env.rbacHeaderInfoMap[filter[i]],env.optionalHeaderListTxInfoCloned)<0){
                                env.optionalHeaderListTxInfoCloned.push(env.rbacHeaderInfoMap[filter[i]]);
                            }
                            var label = env.rbacSideBarLabels[env.rbacHeaderInfoMap[filter[i]]];

                            if(!$('form[name=txInfo] li.tran-'+env.rbacHeaderInfoMap[filter[i]]+' input').is(":checked")){
                                $('.transactions-table .' + env.rbacHeaderInfoMap[filter[i]]).hide();
                                $('.tran-info .check-list').prepend('<li  class="tran-'+env.rbacHeaderInfoMap[filter[i]]+'"><div class="checkbox"><input type="checkbox" class="invisible" value="'+env.rbacHeaderInfoMap[filter[i]]+'" id="lbl-003"><div class="custom-check"></div></div><label for="lbl-003">'+label+'</label></li>');
                            }else{
                                $('.transactions-table .' + env.rbacHeaderInfoMap[filter[i]]).show();
                                env.optionalHeaderListTxInfo = jQuery.grep(env.optionalHeaderListTxInfo, function (value) {
                                    return value != env.rbacHeaderInfoMap[filter[i]];
                                });
                            }
                        }
                        $('.transactions-table .'+env.rbacHeaderInfoMap[filter[i]] + ' .drop-opener').remove();
                    }
                    else {
                        $('.transactions-table .' + env.rbacHeaderInfoMap[filter[i]]).show();
                    }
                }
            }



            if (env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('customerInfo')) {
                if (accessRoles.subHierarchy.customerInfo.accessType == "NONE") {
                    $('.custInfo').remove();
                    $('.slick-arrow').remove();
                    $('#sidebar .slides').slick('unslick');
                    $('reporting-customer-info-slider').remove();
                }
            }

            if (env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('transactionInfo')) {

                if (accessRoles.subHierarchy.transactionInfo.accessType == "NONE") {
                    $('.txInfo').remove();
                    $('.slick-arrow').remove();
                    $('#sidebar .slides').slick('unslick');
                    $('reporting-transaction-info-slider').remove();
                    $.each(env.rbacTxnInfoMap, function (key, value) {
                        env.optionalHeaderListTxInfo.push(value);
                        $('.transactions-table .' + value).remove();
                    });
                }
            }

            if (env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('displayQuickSummary')) {
                if (accessRoles.subHierarchy.displayQuickSummary.accessType == "NONE") {
                    $('reporting-summary-box').remove();
                }
            }



            if (env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('customerInfo')) {
                if (accessRoles.subHierarchy.customerInfo.accessType != "NONE") {
                    var roles = accessRoles.subHierarchy.customerInfo;
                    if (roles.subHierarchy != null) {
                        for (s in roles.subHierarchy) {
                            if (roles.subHierarchy.hasOwnProperty(s)) {
                                if (roles.subHierarchy[s].accessType == "NONE") {
                                    $('.custInfo .cust-' + env.rbacCustInfoMap[s]).remove();
                                    $('.transactions-table .' + env.rbacCustInfoMap[s]).remove();
                                }
                                else if (roles.subHierarchy[s].accessType == "MASKEDVIEW") {
                                    $('.custInfo .cust-' + env.rbacCustInfoMap[s]).find('input[type="checkbox"]').prop("disabled", true);
                                }
                            }
                        }
                    }
                }
            }

            if (env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('transactionInfo')) {
                if (accessRoles.subHierarchy.transactionInfo.accessType != "NONE") {
                    var roles = accessRoles.subHierarchy.transactionInfo;
                    if (roles.subHierarchy != null) {
                        for (s in roles.subHierarchy) {
                            if (roles.subHierarchy.hasOwnProperty(s)) {
                                if (roles.subHierarchy[s].accessType == "NONE") {
                                    $('.txInfo .tran-' + env.rbacTxnInfoMap[s]).remove();
                                    $('.transactions-table .' + env.rbacTxnInfoMap[s]).remove();
                                }
                                else if (roles.subHierarchy[s].accessType == "MASKEDVIEW") {
                                    $('.txInfo .tran-' + env.rbacTxnInfoMap[s]).find('input[type="checkbox"]').prop("disabled", true);
                                }
                            }
                        }
                    }
                }
            }



            if (env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('exportTxnReport')) {
                if (accessRoles.subHierarchy.exportTxnReport.accessType == "NONE") {
                    $('#exportToCSV').remove();
                } else {
                    $('#exportToCSV').show();
                }
            }
            else {
                $('#exportToCSV').show();
            }
            transactionTable.updateTableData();
            searchBox.prepareSearchList(data.transactions);
            if (env.updatedByPagination) {
                env.updatedByPagination = false;
            }
            else {

                env.stopRequest = true;


                if (data.transactions && data.transactions.length != 0) {
                    $('.dataTables_paginate').pagination('selectPage', 1);
                }
                if (data.transactions != null) {
                    if (data.transactions.length > 0) {
                        //  $('.transactions-table-container').style("overflow","auto");
                        paginate.init(data.count, 25);
                        // if (data.quickSummary != null) {
                        //     miniSummary.init(data.quickSummary);
                        // }
                    }

                }
            }
        });

    };

    var closeUserMenu = function closeUserMenu(e) {
        //CLOSE USER MENU
        var userMenu = $('.user');
        if (null != userMenu && undefined != userMenu && !userMenu.is(e.target)) {
            $('.user').removeClass('active');
            userMenu.find('.dropdown-menu').fadeOut();
        }
    }



    var logout =  function logout() {
        txAction.logout().then(function (data) {
            if (data) {
                filters.resetAllFilters();
                transactionTable.resetHeaderInfo();
                window.location.href = env.baseUrl + '/#login';
            }
            else {
                console.log("Logout failed");
                window.location.href = env.baseUrl + '/#login';
            }
        }, function (data) {
            console.log("errror " + data);
            window.location.href = env.baseUrl + '/#login';
        });
    }

    return {
        init: init,
        updateTableData: updateTableData,
        filterDataTable: filterDataTable,
        resetTable: resetTable,
        getTransactionInfo: getTransactionInfo,
        markLastColumn: markLastColumn,
        resetHeaderInfo: resetHeaderInfo
    };

})();


