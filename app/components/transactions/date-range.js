var dateRange = (function () {

    var init = function () {


        var start = moment().subtract(24, 'hours');
        var end = moment();
        var minDate = moment(new Date(2016, 0, 1, 0, 0, 0));
        var maxDate = moment(new Date());

        var ranges = {
            'Last 24 hours': [moment().subtract(24, 'hours'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days').set({ hour: 0, minute: 0, second: 0, millisecond: 0 }), moment()],
            'Last 30 Days': [moment().subtract(29, 'days').set({ hour: 0, minute: 0, second: 0, millisecond: 0 }), moment()],
            'Current Month': [moment().startOf('month').set({ hour: 0, minute: 0, second: 0, millisecond: 0 }), moment()],
            'Last 6 months': [moment().subtract(6, 'months').set({ hour: 0, minute: 0, second: 0, millisecond: 0 }), moment()],
            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        };

        $('.daterangepick').daterangepicker({
            "locale": {
                "format": 'DD-MMM-YYYY'
            },
            "linkedCalendars": false,
            "autoUpdateInput": true,
            "showDropdowns": true,
            "showCustomRangeLabel": false,
            "buttonClasses": "btn",
            "applyClass": "btn-primary",
            "cancelClass": "btn-link",
            "minDate": minDate,
            "maxDate": maxDate,
            "startDate": start,
            "endDate": end
        });

        $('.daterangepick').on('apply.daterangepicker', function (ev, picker) {

            var start = picker.startDate;
            var end = picker.endDate;

            env.getTransactionsRequestObject.startDate = start.format("YYYY-MM-DD HH:mm:ss");
            env.getTransactionsRequestObject.endDate = end.format("YYYY-MM-DD HH:mm:ss");
            $('.dates-list li').removeClass('liSelect');
            filters.applyFilters(this);
        });



        $('.daterangepick').on('cancel.daterangepicker', function (ev, picker) {
            //do something, like clearing an input
            /*   var startDate = moment().subtract(24, 'hours');
               var endDate = moment();
               env.getTransactionsRequestObject.startDate = moment().subtract(24, 'hours').format("YYYY-MM-DD HH:mm:ss");
               env.getTransactionsRequestObject.endDate = moment().format("YYYY-MM-DD HH:mm:ss");
               $('.date-range input').val(startDate.format('DD-MMM-YYYY') + ' - ' + endDate.format('DD-MMM-YYYY'));
               $('.dates-list li').removeClass('liSelect');
               $('.dates-list li:first-child').addClass('liSelect');
               */

        });

        var html_range = "";

        for (var daterange in ranges) {
            if (daterange == 'Last 6 months' || daterange == 'Last Year')
                html_range = html_range + '<li class="disabled">' +
                    '<a href="#" data-range-key="' + daterange + '" >' +
                    '<strong>' + daterange + '</strong>' +
                    '<span>' + ranges[daterange][0].format('DD-MMM-YYYY') + ' - ' + ranges[daterange][1].format('DD-MMM-YYYY') + '</span>' +
                    '</a>' +
                    '</li>';
            else
                html_range = html_range + '<li>' +
                    '<a href="#" data-range-key="' + daterange + '" >' +
                    '<strong>' + daterange + '</strong>' +
                    '<span>' + ranges[daterange][0].format('DD-MMM-YYYY') + ' - ' + ranges[daterange][1].format('DD-MMM-YYYY') + '</span>' +
                    '</a>' +
                    '</li>';



        }
        $(".dates-list").html(html_range);
        $(".date-range input").attr('readonly', true);
        $('.dates-list li:first-child').addClass('liSelect');
        $('.daterangepicker .calendar .prev').on('click', function (event) {
            event.stopImmediatePropagation();
        });
        $('.dates-list li').on('click', 'a', function () {
            if (!$(this).parent().hasClass('disabled')) {
                $('.dates-list li').removeClass("active");
                $('.dates-list li').removeClass('liSelect');
                $(this).addClass("active");
                $(this).parent().addClass('liSelect');
                var range_name = $(this).data("range-key");
                start = ranges[range_name][0];
                end = ranges[range_name][1];
                $('.date-range input').val(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
                env.getTransactionsRequestObject.startDate = start.format("YYYY-MM-DD HH:mm:ss");
                env.getTransactionsRequestObject.endDate = end.format("YYYY-MM-DD HH:mm:ss");
            }
        });
    };

    return {
        init: init
    }
})();
