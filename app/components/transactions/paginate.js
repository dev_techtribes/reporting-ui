
var paginate = (function () {
    var init = function (totalItems, items_displayed_per_page) {
        if (env.index == 1) {
            $(".dataTables_paginate").pagination({
                items: totalItems,
                itemsOnPage: items_displayed_per_page,
                cssStyle: 'dalberry-theme',
                displayedPages: 5,
                ellipsePageSet: false,
                edges: 0,
                onPageClick: function (pageNumber, event) {
                    env.index = (items_displayed_per_page * (parseInt(pageNumber) - 1)) + 1;
                    env.getTransactionsRequestObject.index = env.index;
                    if (!env.stopRequest) {
                        window.scrollTo(0,0);
                            transactionTable.getTransactionInfo();
                    }
                    env.stopRequest=false;
                    env.updatedByPagination = true;
                    var count = $(".dataTables_paginate").pagination('getPagesCount');
                    if (pageNumber == 1 || pageNumber == count) {
                        $('.dataTables_paginate .disabled').closest("li").css('display', "none");
                    }
                    else {
                        $('.dataTables_paginate .disabled').closest("li").css('visibility', "visible")
                    }

                },
                onInit: function () {
                    $('.dataTables_paginate .disabled').closest("li").css('visibility', "hidden");
                }
            });
        }


    };
    return {
        init: init
    }
})()
