"use strict";

var observations = function () {
    var txnRef = "";
    var latestObservationObject = {};

    var jsonSortByDate = function jsonSortByDate(arr) {

        arr.sort(function (left, right) {
            return moment.utc(left.updatedTime).diff(moment.utc(right.updatedTime));
        });

        return arr;
    };

    var getObservations = function getObservations(txnref) {

        var markup = "";
        var prom1 = observationsService.getObservations(txnref);
        var prom2 = transactionService.getTxnLogs(txnref);
        var latestObservation = [];
        latestObservationObject = {};
        $(".comment-form ul.comments").html("");

        Promise.all([prom1, prom2]).then(function (data) {
            if (data[0] != null && data[0] != undefined) {

                if (data[0].length != 0) {
                    var observationsData = jsonSortByDate(data[0]);
                    console.log(observationsData);
                    /*latestObservation = observationsData.splice(-1,1);
                     latestObservationObject = $.extend({},latestObservation[0]);
                     console.log(latestObservation);
                     draw_observations(latestObservation);*/
                }
                if (data[1] != null && data[1] != undefined) {
                    // if(data[1].length != 0 || data[0].length!=1)
                    //{
                    var logData = data[1];
                    var mergedLogDataSorted = mergeLogsAndObservations(logData, observationsData);
                    draw_logs(mergedLogDataSorted);
                    //}
                }
            }
        });
    };

    var init = function init(txnRef) {

        this.txnRef = txnRef;
        initialiseEvents(txnRef);
    };

    var mergeLogsAndObservations = function mergeLogsAndObservations(logsData, observationsData) {

        var mergedLogData = [];
        $.each(logsData, function (index, value) {
            var logItem = {};
            logItem.agent = value.agent;
            logItem.logText = value.action;
            logItem.updatedTime = moment(value.updatedAt).format("YYYY-MM-DD HH:mm:ss");
            //logItem.comment = env.logsCommentNotAvailable;
            if(value.reasonForChange != null){
                logItem.comment = value.reasonForChange;
            }
            else{
                logItem.comment = env.logsCommentNotAvailable;
            }
            
            mergedLogData.push(logItem);
        });

        $.each(observationsData, function (index, value) {
            var observationsItem = {};
            observationsItem.agent = value.agent;
            observationsItem.logText = env.observationsCommentAdded;
            observationsItem.updatedTime = moment(value.updatedTime).format("YYYY-MM-DD HH:mm:ss");
            observationsItem.comment = value.observation;
            mergedLogData.push(observationsItem);
        });

        var mergedLogDataSorted = jsonSortByDate(mergedLogData);

        return mergedLogDataSorted;
    };

    var draw_logs = function draw_logs(observationsData) {
        var markup = "";
        for (var i = 0; i < observationsData.length; i++) {
            markup = markup + "<tr>" + "<td width='20%'>" + moment(observationsData[i].updatedTime).format("YYYY-MM-DD HH:mm:ss") + "</td> " + "<td width='30%'>" + observationsData[i].logText + "</td>" + "<td width='30%'>" + observationsData[i].comment + "</td>" + "<td width='20%'>" + observationsData[i].agent + "</td> </tr>";
        }
        $(".transaction-log tbody").html(markup);
    };

    var draw_observations = function draw_observations(latestObservation) {
        var observationHtml = "<li>" + "<h6>" + latestObservation[0].agent + " " + moment(latestObservation[0].updatedTime).format("YYYY-MM-DD HH:mm:ss") + "</h6>" + "<p>" + latestObservation[0].observation + "</p>" + '<ul class="tools"> <li><button  type="button" class="edit" >edit</button></li> <li><button class="remove" type="button">remove</button></li> <li><button class="cancel" type="button">cancel</button><button class="save" type="button">save</button></li></ul> </li>';

        $(".comment-form ul.comments").html(observationHtml);
        $(".save").hide();
        $(".cancel").hide();
    };

    var makeObservationsEditable = function makeObservationsEditable() {

        var editableText = $("<textarea  maxlength ='200' placeholder='Edit comment' class='form-control edit-area'  cols='1' rows='1' autocomplete='off' resize='none'/>");
        //<textarea maxlength="1000" placeholder="Edit comment" class="form-control add-observation-area" cols="1" rows="1" autocomplete="off"></textarea>
        var backup = $("ul.comments p");
        var observationsContent = backup.html();
        console.log(observationsContent);
        editableText.val(observationsContent);
        $("ul.comments p").replaceWith(editableText);
        editableText.focus();
        $('.save').show();
        $('.edit').hide();
        $('.remove').hide();
        $('.cancel').show();
    };

    var editableTextBlurred = function editableTextBlurred() {
        $('.save').hide();
        $('.cancel').hide();
        $('.edit').show();
        $('.comments .tools .remove').show();
        var html = latestObservationObject.observation;
        var viewableText = $("<p>");
        viewableText.html(html);
        $("textarea.edit-area").replaceWith(viewableText);
        // setup the click event for this new div
        //$(viewableText).click(makeObservationsEditable);
    };

    var initialiseEvents = function initialiseEvents(txnRef) {

        $(".modal").on("hidden.bs.modal", function () {
            $(".add-observation-area").val("");
            $("#observations-area ul.comments").html("");
        });

        $("ul.comments").off("click", ".edit").on("click", '.edit', makeObservationsEditable);
        $("ul.comments").off("click", ".cancel").on("click", '.cancel', editableTextBlurred);

        $("div.comment-form").off("click", ".remove").on("click", ".remove", function (e) {

            //get observations
            //get latest
            // load observations
            // get logs
            // merge with remaining observations
            var transactionReference = latestObservationObject.transactionReference;
            var observationsId = latestObservationObject.id;
            observationsService.deleteObservations(transactionReference, observationsId).then(function (data) {

                getObservations(txnRef);
            });
        });

        $("ul.comments").off("click", ".save").on("click", ".save", function (e) {
            //get observations
            //get latest
            // load observations
            // get logs
            // merge with remaining observations
            console.log(latestObservationObject);
            var transactionReference = latestObservationObject.transactionReference;
            var observationsId = latestObservationObject.id;
            var content = $("ul.comments textarea.edit-area").val();
            observationsService.editObservations(transactionReference, observationsId, content).then(function (data) {

                getObservations(txnRef);
            });
        });

        $("div.comment-form").off("click", ".add-observation").on("click", ".add-observation", function (e) {
            //send value
            // load it to observation area
            // refresh logs
            console.log(txnRef);
            var content = $('div.comment-form textarea.add-observation-area').val().trim();

            observationsService.insertObservations(txnRef, content).then(function () {
                getObservations(txnRef);
                $('div.comment-form textarea.add-observation-area').val("");
                $('.add-observation').attr('disabled', 'disabled');
            });
        });
    };

    return {
        init: init,
        getObservations: getObservations
    };
}();
