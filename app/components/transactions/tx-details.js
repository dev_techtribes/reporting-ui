var transactionDetails = (function () {
    var tx;
    var txnObject;
    var custObject;
    var setblform = function (data) {
        $("#bl-partial").text('');
        if (data.isBlacklist) {
            $("#bl-lbl").text("Remove from");
            $("#bl-head").text("remove from");
            tx.transactionInfo.isBlacklist = true;
        }
        else if (data.isPartiallyBlacklist) {
            tx.transactionInfo.isBlacklist = false;
            $("#bl-lbl").text("Link to");
            $("#bl-head").text("link to");
            var s = "";
            var txt = "already blacklisted."
            count = 0;
            if (data.email != null) {
                s = 'Email';
                count++;
            }
            if (data.cc != null) {
                s != "" ? s = s + ' and ' + 'Credit card' : s = 'Credit card';
                count++;
            }
            if (data.macAddress != null) {
                s != "" ? s = s + ' and ' + 'Mac address' : s = 'Mac address';
                count++;
            }
            if (count == 1)
                var verb = ' is ';
            else
                var verb = ' are ';
            $("#bl-partial").text(s + verb + txt);
        }
        else {
            tx.transactionInfo.isBlacklist = false;
            $("#bl-lbl").text("Add to");
            $("#bl-head").text("add to");
        }
    }
    var populateTransactionData = function (txnObject) {
        var is3d = txnObject.is3d;
        var flag3d = "";
        if(is3d == null || is3d == '' || is3d == 'N/A'){
            flag3d = "N/A";
        }
        if(is3d == true){
            flag3d = "Yes";
        }
        else{
            flag3d = "No";
        }

        /*if (is3d) {
            flag3d = "Yes";
        }
        else if (is3d == 0) {
            flag3d = "No";
        }
        else {
            flag3d = "N/A";
        }*/

        $('#tx-merchant').text(txnObject.merchant);
        $('#tx-brand').text(txnObject.brand);
        $('#tx-amt').val(txnObject.txnAmount);
        $('#tx-ref').text(txnObject.dalberryTxnReference);
        $('#tx-merchantTxnId').text(txnObject.merchantTxnId);
        $('#tx-type').text(txnObject.txnType);
        $('#tx-status').text(txnObject.txnStatus);
        $('#tx-cdate').text(txnObject.txnCreatedDate);
        $('#tx-pdate').text(txnObject.txnProcessedDate);
        $('#tx-amt-lbl').text(formatAmounts(txnObject.txnAmount));
        $('#tx-curr').text(txnObject.txnCurrency);
        $('#tx-name').text(txnObject.cardHolderName);

        $('#tx-method').html('<div class="modalPaymentMethod payment-table-logo '+txnObject.cardType.toLowerCase()+'" data-original-title="" title=""></div>');
        $('#tx-insType').text(txnObject.paymentMethod);
        $('#tx-ins').text(txnObject.paymentInstrument);
        $('#tx-mid').text(txnObject.mid);
        $('#tx-ip').text(txnObject.ip);
        $('#tx-is3D').text(flag3d);
        $('#tx-descriptor').text(txnObject.billingDescriptor);
        $('#tx-rev').text(txnObject.revertedTxn);
        $('#tx-platform').text(txnObject.platform);
       /* $('#tx-browser').text(txnObject.browser);*/
        $('#tx-ptxnid').text(txnObject.providerTxnId);
        $('#tx-authcode').text(txnObject.authCode);
        $('#tx-errorcode').text(txnObject.errorCode);
        $('#tx-msg').text(txnObject.errorMessage);
        $('#tx-dlb-error').text(txnObject.dbErrorCode);
        $('#tx-dlb-msg').text(txnObject.dbErrorMessage);
        $('#tx-channel').text(txnObject.channel);
        $('#tx-subchannel').text(txnObject.subChannel);
    };
    var populateCustomerData = function (custObject) {
        var isMaleFlag = custObject.isMale;
        //var accessRoles = env.tempCollection.accessPermissionHeirarchy;

        var gender = "";
        if(isMaleFlag == "N/A" || isMaleFlag == null){
            gender = "N/A";
        }
        else{
           if (isMaleFlag) {
                gender = "M";
            }
            else {
                gender = "F";
            }
        }
        
        /*var birthDate = ""
        if(custObject.birthDate != "****")
        {
            var dob =  new Date(custObject.birthDate.replace('.','/').replace('.','/'));
            birthDate = moment(dob).format("DD:MMM:YYYY");
        }
        else
        {
            birthDate = c.birthDate;
        }*/
        $('#tx-custId').text(txnObject.merchantCustomerId);
        $('#tx-custfName').text(custObject.firstName);
        $('#tx-custlName').text(custObject.lastName);
        $('#tx-custEmail').html('<a>' + custObject.customerEmail + '</a>');
        $('#tx-custphone').text(custObject.phone);
        $('#tx-custdob').text(custObject.birthDate);
        $('#tx-custgender').text(gender);
        $('#tx-custaddress').text(custObject.address);
        $('#tx-custcity').text(custObject.city);
        $('#tx-custState').text(custObject.state);
        $('#tx-custZip').text(custObject.postalCode);
        $('#tx-custCountry').text(custObject.country);
        $('#tx-mac').text(custObject.macAddress);
        $('#tx-issuingBank').text("");
        $('#tx-issuingCountry').text("");
    }

    var loadTransactionModal = function (txDetails) {
        tx = txDetails;
        var accessRoles = env.tempCollection.accessPermissionHeirarchy;
        if ( env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('addTxnObservations'))
        {
            if(accessRoles.subHierarchy.addTxnObservations.accessType == "NONE"){
                $('observations-area').remove();
            }
        }
        if ( env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('displayTxnLogs'))
        {
            if(accessRoles.subHierarchy.displayTxnLogs.accessType == "NONE"){
                $('.info-table').remove();
            }
        }
        if ( env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('transactionInfo'))
        {
            if(accessRoles.subHierarchy.transactionInfo.accessType == "MASKEDVIEW")
                rbac.blockElement($('table.tranInfo'));

            if(accessRoles.subHierarchy.transactionInfo.accessType == "NONE"){
                $('.modal-body .tranInfo').remove();
            }
            else{
                rbac.updateTransactionInfo(accessRoles.subHierarchy.transactionInfo);
            }
        }

        if ( env.tempCollection && accessRoles && accessRoles.subHierarchy != null && accessRoles.subHierarchy.hasOwnProperty('customerInfo'))
        {
            if(accessRoles.subHierarchy.customerInfo.accessType == "MASKEDVIEW")
                rbac.blockElement($('table.custInfo'));

            if(accessRoles.subHierarchy.customerInfo.accessType == "NONE"){
                $('.modal-body .custInfo').remove();
            }
            else{
                rbac.updateCustomerInfo(accessRoles.subHierarchy.customerInfo);
            }
        }
        observations.init(txDetails.transactionInfo.dalberryTxnReference);
        observations.getObservations(txDetails.transactionInfo.dalberryTxnReference);

        txnObject = txDetails.transactionInfo;
        custObject = txDetails.customerInfo;
        if (txnObject.txnType == 'Sale' && txnObject.txnStatus == "Approved") {
            $("#refund-box").show();
            if((accessRoles && accessRoles.subHierarchy && accessRoles.subHierarchy.processRefundTxn && accessRoles.subHierarchy.processRefundTxn.accessType == 'NONE') || tx.transactionInfo.isRefundTxn == true)
            {
                $("#refund-btn").attr("disabled", "disabled");
            }
            else if (!tx.transactionInfo.isRefundTxn)
            {
                $("#refund-btn").removeAttr("disabled");
            }

        }
        else {
            $("#refund-box").hide();
        }
        if (txnObject.txnType == 'Sale' && txnObject.txnStatus == "Approved") {

            $("#chargeBackButton").show();
            //(accessRoles && accessRoles.subHierarchy && accessRoles.subHierarchy.processChargeback && accessRoles.subHierarchy.processChargeback.accessType == 'NONE') || tx.transactionInfo.isChargebackTxn == true
            if((accessRoles && accessRoles.subHierarchy && accessRoles.subHierarchy.processChargeback && accessRoles.subHierarchy.processChargeback.accessType == 'NONE') || tx.transactionInfo.chargebackTxn == true)
            {
                $("#chargeBackButton").attr("disabled", "disabled");
            }
            else if (!tx.transactionInfo.chargebackTxn)
            {
                $("#chargeBackButton").removeAttr("disabled");
            }
        }
        else
        {

            $("#chargeBackButton").hide();
        }
        if(accessRoles && accessRoles.subHierarchy && accessRoles.subHierarchy.processUpdateTxnDetails && accessRoles.subHierarchy.processUpdateTxnDetails.accessType == 'NONE')
        {
            $("#txn-update-btn").hide();
        }
        else{
            $("#txn-update-btn").show();
        }
        populateCustomerData(custObject);
        populateTransactionData(txnObject);
        transactionService.getBlackListParameters({
            dalberryTxnReference: txnObject.dalberryTxnReference
        }).then(function (data) {
            setblform(data);
        });

        if((accessRoles && accessRoles.subHierarchy && accessRoles.subHierarchy.processFraudTxn && accessRoles.subHierarchy.processFraudTxn.accessType == 'NONE') || tx.transactionInfo.isFraud == true)
        {
            $("#fraud-btn").attr("disabled", "disabled");
        }
        else if (!tx.transactionInfo.isFraud)
        {
            $("#fraud-btn").removeAttr("disabled");
        }
        //setblform1(tx.transactionInfo.isBlacklist);
        $('.refund-error').text('');
        $('.refund-success').text('');
        $('.txn-update-error').text('');
        $('.txn-update-success').text('');
        $('.bl-success').text('');
        $('.bl-error').text('');
        $('.f-success').text('');
        $('.f-error').text('');
        $('.chargeback-error').text('');
        $('.chargeback-success').text('');
        $('#tx-details-modal').modal({backdrop: 'static', keyboard: false});
        //IE 10 placeholder fix
        $('.add-observation-area').val('');
    };
    var initialiseEvents = function () {
        $(".close-modal").click(function () {
            $(this).parents('.modal').first().modal("hide");
        });

        $('#fraudReason').on('change', function(){
           if(!$('#fraudReason').val() == "0"){
            $('#fraud-result').text("");
           }
        });

        $('#fraud-modal').on('hide.bs.modal', function (e) {
            $('#fraudReason').val('0');
            //$('#fraudReason').selectpicker('refresh');
        });

        $('.tranInfo').on("click","#chargeBackButton",function () {

            if($('#chargeBackButton').attr('disabled') == 'disabled'){
                return false;
            }

  /*           $(".modal-backdrop").fadeOut("slow");

             $(".modal-backdrop").remove();*/
          /*  $("#tx-details-modal").removeClass("fade").modal("hide");*/
           // $("#dialog2").modal("show").addClass("fade");


                chargeback.init(tx);




        });

        $("#refund-process").click(function () {
            var d = tx.transactionInfo;
            txAction.refund({
                transactionReference: d.dalberryTxnReference,
                amount: $('#tx-amt').val()
            }).then(function (data) {
                d.isRefundTxn = true;
                $("#refund-btn").attr("disabled", "disabled");

                notificationService.notifySuccess('Request Successful!')
                $("#refund-modal").modal('hide');
                $('#loader-container').fadeOut('fast');
                env.getTransactionsRequestObject.index=1;
                transactionTable.getTransactionInfo();
            }, function (data) {
                notificationService.notifyDanger('Request Failed');
                $("#refund-modal").modal('hide');
            });
        });
        $("#txn-update-details").click(function () {
            var d = tx.transactionInfo;
            var reasonForChange = $("#txn-reason-change").val();
            if(reasonForChange == null || reasonForChange == '' || reasonForChange.trim().length < 4){
                $('.update-txn-modal-error').text('Reason is required');
                return;
            }
            else{
                $('.update-txn-modal-error').text('');
            }

            var oldTxnStatus = $('#tx-status').text();
            var oldProviderTxnId = $('#tx-ptxnid').text();
            var oldAuthCode = $('#tx-authcode').text();
            var oldIs3dSecure = false;
            if($('#tx-is3D').text() == "Yes"){
                oldIs3dSecure = true;
            }
            else{
                oldIs3dSecure = false;
            }

            var providerTxnId = $("#txn-provider-txn-id").val();
            var authCode = $("#txn-auth-code").val();
            var txnStatusId = $("#available-txn-status").val();
            var responseMessageId = $("#provider-errors").val();
            var supplerErrorMessage = $( "#provider-errors option:selected" ).text();
            var is3dSecure = "false";
            if($("#txn-3d-secure").prop('checked') == true){
                is3dSecure = "true";
            }
           else{
              is3dSecure = "false";
           }
            txAction.updatetxnstatus({
                transactionReference: d.dalberryTxnReference,
                providerTxnId: providerTxnId,
                authCode: authCode,
                newTxnStatusId: txnStatusId,
                action: "insert",
                reasonForChange: reasonForChange,
                is3dSecure: is3dSecure,
                oldTxnStatus: oldTxnStatus,
                oldProviderTxnId: oldProviderTxnId,
                oldAuthCode: oldAuthCode,
                oldIs3dSecure: oldIs3dSecure,
                responseMessageId: responseMessageId,
                supplerErrorMessage: supplerErrorMessage

            }).then(function (data) {
                //$("#txn-update-btn").attr("disabled", "disabled");
               if(data.status == "SUCCESS")
               {
                   notificationService.notifySuccess('Update Successful');
                   $("#txnstatus-modal").modal('hide');
                   $('#loader-container').fadeOut('fast');
                   if(data.providerTxnId != null && data.providerTxnId != ''){
                       $('#tx-ptxnid').text(data.providerTxnId);
                   }
                   else{
                       $('#tx-ptxnid').text('N/A');
                   }
                   if(data.authCode != null && data.authCode != ''){
                       $('#tx-authcode').text(data.authCode);
                   }
                   else{
                       $('#tx-authcode').text('N/A');
                   }
                   $('#tx-status').text(data.txnStatus);
                   if(data.is3dSecure == true){
                       $("#tx-is3D").text('Yes');
                   }
                   else{
                       $("#tx-is3D").text('No');
                   }

                   observations.getObservations(d.dalberryTxnReference);
                   transactionTable.getTransactionInfo();

                   var accessRoles = env.tempCollection.accessPermissionHeirarchy;
                   if (txnObject.txnType == 'Sale' && data.txnStatus == "Approved") {

                       $("#chargeBackButton").show();
                       //(accessRoles && accessRoles.subHierarchy && accessRoles.subHierarchy.processChargeback && accessRoles.subHierarchy.processChargeback.accessType == 'NONE') || tx.transactionInfo.isChargebackTxn == true
                       if((accessRoles && accessRoles.subHierarchy && accessRoles.subHierarchy.processChargeback && accessRoles.subHierarchy.processChargeback.accessType == 'NONE') || txnObject.chargebackTxn == true)
                       {
                           $("#chargeBackButton").attr("disabled", "disabled");
                       }
                       else if (!tx.transactionInfo.chargebackTxn)
                       {
                           $("#chargeBackButton").removeAttr("disabled");
                       }
                   }
                   else
                   {

                       $("#chargeBackButton").hide();
                   }
               }
              else
               {
                   notificationService.notifyDanger('Update transaction is failed');
               }

            }, function (data) {
                notificationService.notifyDanger('Update transaction is failed');
            });
        });
        $("#bl-process").click(function () {
            var d = tx.transactionInfo;
            txAction.blacklist({
                ref: d.dalberryTxnReference,
                comments: $('#bl-comment').val()
            }).then(function (data) {
                tx.transactionInfo.isBlacklist = true;
                $("#bl-lbl").text("Remove from");
                $("#bl-head").text("remove from");
                $('.bl-error').text('');
                if (tx.transactionInfo.isBlacklist)
                    notificationService.notifySuccess('Added to Blacklist.');
                else
                    notificationService.notifySuccess('Removed from Blacklist.');
                $("#blacklist-modal").modal('hide');
            }, function (data) {
                $("#blacklist-modal").modal('hide');

                notificationService.notifyDanger('Request Failed');

            });
        });

        $("#refund-btn").click(function () {
            if($('#refund-btn').attr('disabled') == 'disabled'){
                return false;
            }
            var d = tx.transactionInfo;
            var c = tx.customerInfo;
            $('.refund-error').text('');
            $('.refund-success').text('');
            var amt = parseFloat($('#tx-amt').val());
            if ($('#tx-amt').val() == '' || $('#tx-amt').val() != amt || amt > d.txnAmount) {
                $('.refund-error').text('Invalid value');
                return;
            }
            $("#rfd-amt").text(d.txnCurrency + ' ' + $('#tx-amt').val());
            $("#rfd-rx").text(c.firstName + ' ' + c.lastName);
            $("#rfd-ref").text(d.dalberryTxnReference);
            $("#rfd-txn").text(d.merchantTxnId);

            $("#refund-modal").modal({backdrop: 'static', keyboard: false});
        });

        $('#available-txn-status').on('change', function(){
            var txnStatusId = $("#available-txn-status").val();
            var providerErrors = "";
            if(txnStatusId == null || txnStatusId == ''){
                $('#provider-errors').attr("disabled", "disabled");
                $("#provider-errors").html('');
            }
            else{
                txAction.providerErrorList({
                txnStatusId: txnStatusId
            }).then(function (data) {
                env.getProviderErrorsData = data.providerErrorList.slice();
                if(env.getProviderErrorsData != null && env.getProviderErrorsData !=''){
                    $('#provider-errors').removeAttr('disabled');
                    providerErrors += '<option value=""></option>';
                    for(var i=0;i< env.getProviderErrorsData.length;i++){
                         providerErrors += '<option value="'+ env.getProviderErrorsData[i].responseMessageId + '">' + env.getProviderErrorsData[i].supplerErrorMessage + '</option>';
                    }
                    $('#provider-errors').html(providerErrors);
                    //$('#provider-errors').selectpicker('refresh');
                }
                else{
                    $('#provider-errors').attr("disabled", "disabled");
                }
                console.log(providerErrors);
                $('#provider-errors').html(providerErrors);
               // $('#provider-errors').selectpicker('refresh');
            });
            }

        });

        $("#txn-update-btn").click(function () {
            var d = tx.transactionInfo;
            var c = tx.customerInfo;
             var txnStatus = $('#tx-status').text();
            var statusList = "";
             $('.update-txn-modal-error').text('');
            $("#txn-reason-change").val('');
            $('#provider-errors').html('');
            $('#provider-errors').attr("disabled", "disabled");
          //  $('#provider-errors').selectpicker('refresh');
            txAction.availableTxnStatusList({
                txnType: d.txnType,
                txnStatus: txnStatus
            }).then(function (data) {
                env.getAvailableTxnMasterData = data.slice();
                if(env.getAvailableTxnMasterData != null && env.getAvailableTxnMasterData !=''){
                    $('#available-txn-status').removeAttr('disabled');
                    statusList += '<option value=""></option>';
                    for(var i=0;i< env.getAvailableTxnMasterData.length;i++){
                         statusList += '<option value="'+ env.getAvailableTxnMasterData[i].id + '">' + env.getAvailableTxnMasterData[i].status + '</option>';
                    }
                    $('#available-txn-status').html(statusList);
                   // $('#available-txn-status').selectpicker('refresh');
                }
                else{
                    $('#available-txn-status').attr("disabled", "disabled");
                     $('#provider-errors').attr("disabled", "disabled");
                }
                console.log(statusList);
                $('#available-txn-status').html(statusList);
              //  $('#available-txn-status').selectpicker('refresh');
            });


            $('.txn-update-error').text('');
            $('.txn-update-success').text('');
            $("#txn-type").text(d.txnType);
            var status;
            if (txnStatus == 'Approved' || txnStatus == 'Authorized' || txnStatus == 'Cancelled' || txnStatus.indexOf("Completed") != -1) {
                status = '<span class="updatetxnstatus approved">' + txnStatus + '</span>';
            }
            else if (txnStatus.indexOf('In progress') != -1  || txnStatus.indexOf("Initiated") != -1 || txnStatus.indexOf("Pending") != -1) {
                status = '<span class="updatetxnstatus pending">' +  txnStatus + '</span>';
            }
            else if (txnStatus == 'Failed' || txnStatus == 'Declined' || txnStatus == 'GW Decline' || txnStatus == 'Abandoned' || txnStatus == 'Authorize Expired' || txnStatus == 'Expired' || txnStatus.indexOf("Rejected") != -1 || txnStatus.indexOf("Decline") != -1) {
                status = '<span class="updatetxnstatus declined">' + txnStatus + '</span>';
            }
            else {
                status = '<span class="updatetxnstatus">' + txnStatus + '</span>';
            }

            $("#txn-status").html(status);

            /*$("#txn-status").text(d.txnStatus);
            if(d.is3d == true){
                $("#txn-3d-secure").attr('checked', true);
            }
            else{
                $("#txn-3d-secure").attr('checked', false);
            }
            if(d.providerTxnId == 'N/A'){
                $("#txn-provider-txn-id").val('');
            }
            else{
                $("#txn-provider-txn-id").val(d.providerTxnId);
            }
            if(d.authCode == 'N/A'){
                $("#txn-auth-code").val('');
            }
            else{
                $("#txn-auth-code").val(d.authCode);
            }*/
            //$("#txn-status").text(d.txnStatus);
            if($('#tx-is3D').text() == "Yes"){
                $("#txn-3d-secure").attr('checked', true);
            }
            else{
                $("#txn-3d-secure").attr('checked', false);
            }
            $("#txn-provider-txn-id").val($('#tx-ptxnid').text());
            $("#txn-auth-code").val($('#tx-authcode').text());
            $("#txn-reason-change").text('');
            $("#txnstatus-modal").modal({backdrop: 'static', keyboard: false});
        });
        $("#blacklist-btn").click(function () {
            // $('#bl-comment').val('');
            if (!tx.transactionInfo.isBlacklist) {
                var d = tx.transactionInfo;
                var c = tx.customerInfo;
                // $('#bl-date').text(d.txnCreatedDate);
                //$('#bl-cid').text(d.merchantCustomerId);
                $('#bl-email').text(c.customerEmail);
                $('#bl-name').text(c.firstName + ' ' + c.lastName);
                $('#bl-ins').text(d.paymentInstrument);
                $('#bl-address').text(c.address + " " + c.city + ' ' + c.state + ' ' + c.country + ' ' + c.postalCode);
                $("#blacklist-modal").modal({backdrop: 'static', keyboard: false});
            }
        });
        $("#fraud-btn").click(function () {
            $('#fraud-result').text("");
            if($('#fraud-btn').attr('disabled') == 'disabled'){
                return false;
            }
            var d = tx.transactionInfo;
            $("#fraud-modal").modal({backdrop: 'static', keyboard: false});
        });

        $("#fraud-process").click(function () {
            var d = tx.transactionInfo;
            if($('#fraudReason').val() == "0"){
                $('#fraud-result').text("Please select any fraud reason!");
                return;
            }
                txAction.fraud({
                    transactionReference: d.dalberryTxnReference,
                    fraudId:$('#fraudReason').val()
                }).then(function (data) {
                    tx.transactionInfo.isFraud = true;
                    /*$('.f-success').text('Transaction is marked as fraud.');*/
                    notificationService.notifySuccess('Transaction is marked as fraud')
                    $('.f-error').text('');
                    $("#fraud-btn").attr("disabled", "disabled");
                    $("#row_"+tx.index).find('*').filter('.ico-doc').removeClass('ico-doc').addClass('ico-notif-red');
                    $("#fraud-modal").modal('hide');
                    observations.getObservations(tx.transactionInfo.dalberryTxnReference);
                }, function (data) {
                    tx.transactionInfo.isFraud = false;
                    $('.f-success').text('');
                    notificationService.notifyDanger('Request Failed.')
                   // $("#fraud-modal").modal('hide');
                });

        });

        /*$("#fraud-btn").click(function () {
            if($('#fraud-btn').attr('disabled') == 'disabled'){
                return false;
            }
            var d = tx.transactionInfo;
            if (confirm("Are you sure you want to mark this transaction as fraud?")) {
                txAction.fraud({
                    ref: d.dalberryTxnReference,
                }).then(function (data) {
                    tx.transactionInfo.isFraud = true;
                    $('.f-success').text('Transaction is marked as fraud.');
                    $('.f-error').text('');
                    $("#fraud-btn").attr("disabled", "disabled");
                    $("#row_"+tx.index).find('*').filter('.ico-doc').removeClass('ico-doc').addClass('ico-notif-red');
                }, function (data) {
                    tx.transactionInfo.isFraud = false;
                    $('.f-success').text('');
                    $('.f-error').text('Request Failed.');
                });
            }
        });*/
    }
    var init = function () {
        initialiseEvents();
        $('.comment-form textarea').textPlaceholder();
    };

    return {
        init: init,
        loadTransactionModal: loadTransactionModal
    }
})();
