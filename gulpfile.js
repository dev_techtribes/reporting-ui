var gulp = require("gulp");
var dependencies = require("./app/dependencies.js");
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var refresh = require('gulp-livereload');
var lr = require('tiny-lr');
var less = require('gulp-less');
var merge = require('gulp-merge');
var rev = require('gulp-rev');
var deleteFile = require('gulp-remove-files');
//var  merge = require('gulp-sequence');
//var server = lr();
var minifyCSS = require('gulp-minify-css');
var replace = require('gulp-replace');
var server = require('gulp-server-livereload');
//console.log(dependencies);
gulp.task('scripts', function () {
    return gulp.src(dependencies)
        .pipe(replace("/app/templates", "/html"))
        .pipe(concat('reporting.js'))
       // .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
        .pipe(refresh(server));
});
// gulp.task('register', function () {
//     return gulp.src(["app/utilities/importHTML.js", "app/common/directives.js", "app/utilities/polyfill2.js"])
//         .pipe(replace("/app/templates", "/html"))
//         .pipe(gulp.dest('./dist/js'))
//         .pipe(refresh(server));
// });

/** ---- Production Deployment Task Starts Here--------------- **/
gulp.task("prod", function () {
    return gulp.src('dist/**/*')
        .pipe(gulp.dest('./prod'));
});

gulp.task("domain", ["prod"], function () {
     var name = "http://dlb-reporting.cloudapp.net/reportingshiro-1.0";
     //var name = "https://businessdr.dalberry.com/reportingshiro-1.0";
     //var name = "http://localhost:8080/reportingshiro-1.0";
    //var name = "http://10.2.0.11:8080/reportingshiro-1.0";
    return gulp.src(['prod/js/directives.js', 'prod/js/reporting.js'])
        .pipe(replace("buildurl=''", "buildurl='" + name + "'"))
        .pipe(replace("'/api'", '"' + name + '"'))
        .pipe(gulp.dest('./prod/js'));
});

gulp.task('jsversion', ['domain'], function () {
    return gulp.src('prod/js/reporting.js')
       // .pipe(uglify())
        .pipe(rev())
        .pipe(gulp.dest('./prod/js'))
        .pipe(rev.manifest())
        .pipe(gulp.dest('./prod'));
});
gulp.task('cssversion', ['jsversion'], function () {
    return gulp.src('prod/css/reporting.css')
        .pipe(rev())
        .pipe(gulp.dest('./prod/css'))
        .pipe(rev.manifest({
            cwd: './prod',
            merge: true
        }))
        .pipe(gulp.dest('./prod'));
});
gulp.task('replace-versions', ['cssversion'], function () {
    var mf = require('./prod/rev-manifest.json');
    var index = gulp.src('prod/index.html');
    for (s in mf) {
        index = index.pipe(replace(s, mf[s]));
    }
    return index.pipe(gulp.dest('./prod'));

});
gulp.task('remove-extra', ['replace-versions'], function () {
    gulp.src(['./prod/js/reporting.js','./prod/css/reporting.css','./prod/rev-manifest.json'])
        .pipe(deleteFile());
})
gulp.task('versions', ['remove-extra'], function () {

});
gulp.task("deploy", ["versions"]);
/** ---- Production Deployment Task Ends Here--------------- **/
gulp.task('fonts', function () {
    gulp.src('fonts/**/*')
        .pipe(gulp.dest('dist/fonts'));
});
gulp.task('images', function () {
    gulp.src('images/**/*')
        .pipe(gulp.dest('dist/images'));
});
gulp.task('favicon', function () {
    gulp.src('favicon.ico')
        .pipe(gulp.dest('dist'));
});

gulp.task('styles', function () {
    var cssStream = gulp.src('css/**/*.css')
        .pipe(concat('reporting.css'));
    var lessStream = gulp.src(['css/**/*.less'])
        .pipe(less())
        .pipe(concat('less-files.less'));
    var merged = merge(cssStream, lessStream);

    // merged.add(lessStream);

    var mergedStream = merged
        .pipe(concat('reporting.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('dist/css'))
        .pipe(refresh(server));
});
gulp.task('index', function () {
    gulp.src('index.html')
        .pipe(gulp.dest('dist'))
        .pipe(refresh(server));
});

gulp.task('lr-server', function () {
    // server.listen(8081, function(err) {
    //     if(err) return console.log(err);
    // });
    gulp.src('dist')
        .pipe(server({
            livereload: true,
            port: 8081
        }));
});

gulp.task('html', function () {
    gulp.src("app/templates/**/*.html")
        // .pipe(embedlr())
        .pipe(gulp.dest('dist/html'))
        .pipe(refresh(server));
})

gulp.task('default', function () {
    gulp.run('lr-server', 'scripts', 'styles', 'html', 'fonts', 'images', 'favicon', 'index');

    gulp.watch('app/**/*.js', function (event) {
        gulp.run('scripts');
    })

    gulp.watch('css/**/*.css', function (event) {
        gulp.run('styles');
    })

    gulp.watch('css/**/*.less', function (event) {
        gulp.run('styles');
    })

    gulp.watch('app/**/*.html', function (event) {
        gulp.run('html');
    })
    gulp.watch('index.html', function (event) {
        gulp.run('index');
    })
});
